<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wezu_ui' );

/** MySQL database username */
define( 'DB_USER', 'wezu_ui' );

/** MySQL database password */
define( 'DB_PASSWORD', 'abcd1234' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7-QnKa=|9s6u~R$8/#7Zg@l}*=|uO@nD@3BPiQ7;:@o#JQ8$5;-.|HV##-`)u~5/' );
define( 'SECURE_AUTH_KEY',  'MUfgS8^E9vM1bx~A(^tr3Ni[K$5LRl}a113bI,)/n;d:?BbvC21HsVnNu3w!>6j7' );
define( 'LOGGED_IN_KEY',    'oX,rw3uz$XG3UFlG.eP+2OH`My-gmcaymb~@w?Bt)Gzj?c9}L]lC5AYl>MVZKp&Z' );
define( 'NONCE_KEY',        '0<1eMwP7}FL?|kx23l[@@4/4@1U^GPC::3NPk3;8d]5/j(~,>8xA;!&/@PR$n2]h' );
define( 'AUTH_SALT',        'am*}5|$V(}JD)IdCp}?EM=muF9o/[Qb[ZD_1}^J/?Yc:$mF2p.~NBlxBEDa.P1q|' );
define( 'SECURE_AUTH_SALT', 'z8(1fMJMmdv5}a?K_i`hTcoPfX:g)8pPld`3lTjOnH+5c,%{`1F4/+cj7j@ryQ<,' );
define( 'LOGGED_IN_SALT',   'pFYy*kB.f[An-~F#5TB#u5/A<~!~?Ux#XZ)M<v=wUFLJL4*)vIcqjrNdlOy)BIX6' );
define( 'NONCE_SALT',       'Bj#a;}07-w}6JM{,O(>^`HF+vADRc$lkJMXp1yEq4hVc{lTb d@TCQ5ynz2(5yg ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

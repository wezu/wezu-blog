<?php
/* Add extra type on visual composer */
require_once ZU_INCLUDES.'types/zu_template.php';
/* Get List Shortcodes From Folder*/
require_once ZU_DIR . '/shortcodes/zu_base.php';
require_once ZU_DIR . '/shortcodes/zu_carousel.php';
require_once ZU_DIR . '/shortcodes/zu_grid.php';
require_once ZU_DIR . '/shortcodes/zu_fancybox.php';
require_once ZU_DIR . '/shortcodes/zu_btn.php';
require_once ZU_DIR . '/shortcodes/zu_service.php';
require_once ZU_DIR . '/shortcodes/vc_params.php';
require_once ZU_DIR . '/shortcodes/zu_woocommerce.php';
require_once ZU_DIR . '/shortcodes/zu_googlemap.php';
require_once ZU_DIR . '/shortcodes/zu_video.php';

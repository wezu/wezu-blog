(function ($) {
    "use strict";
    $(document).ready(function () {
        $(".zu-carousel").each(function () {
            var $this = $(this), slide_id = $this.attr('id'), slider_settings = zuCarousel[slide_id];
            $this.addClass('owl-carousel owl-theme');
            $this.owlCarousel(slider_settings);
        });
    });
    $(window).load(function(){
        $('.zu-carousel-filter a').click(function(e){
            e.preventDefault();
            var parent = $(this).closest('.zu-carousel-wrap');
            $('.zu-carousel-filter a').removeClass('active');
            var filter = $(this).data('group');
            $(this).addClass('active');
            ZuCarouselFilter( filter, parent );
        });
    });

    /**
     * Carousel Filter
     * @param filter category
     * @param parent
     */
    function ZuCarouselFilter( filter, parent ){
        if ( filter === 'all'){
            $('.zu-carousel-filter-hidden .zu-carousel-filter-item', parent).each(function(){
                var owl   = $(".zu-carousel", parent);
                var parentElem      = $(this).parent(),
                    elem = parentElem.html();
                owl.trigger('add.owl.carousel', [elem]).trigger('refresh.owl.carousel');
                parentElem.remove();
            });
        } else {
            $('.zu-carousel-filter-hidden .zu-carousel-filter-item.'+ filter, parent).each(function(){
                var owl = $(".owl-carousel", parent);
                var parentElem      = $(this).parent(),
                    elem = parentElem.html();
                owl.trigger('add.owl.carousel', [elem]).trigger('refresh.owl.carousel');
                parentElem.remove();
            });

            $('.zu-carousel .zu-carousel-filter-item:not(".'+filter+'")', parent)
                .each(function(){
                var owl   = $(".zu-carousel", parent);
                var parentElem = $(this).parent(),
                    targetPos = parentElem.index();
                $( parentElem ).clone().appendTo( $('.zu-carousel-filter-hidden', parent) );
                owl.trigger('remove.owl.carousel', [targetPos]).trigger('refresh.owl.carousel');
            });
        }
    }
})(jQuery);
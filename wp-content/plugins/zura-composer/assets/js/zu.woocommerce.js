jQuery(document).ready(function () {
    jQuery('#filter-result .slide-product').slick(zu_woocommerce.slick_config);
});

jQuery(document).on('click', '.list-cat-filter li a', function (e) {
    jQuery('.list-cat-filter li a').removeClass('active');
    jQuery(this).addClass('active');
    var term_id = jQuery(this).data('term-id');
    ajaxProductByTermID(term_id);
    e.preventDefault();
});

var ajaxProductByTermID = function (term_id) {
    var filterHeight = jQuery('#filter-result').height();
    jQuery.ajax({
        url: zu_woocommerce.ajax_url,
        dataType: 'json',
        type: 'post',
        data: {
            action: 'zu_woocommerce',
            term_id: term_id,
            args: zu_woocommerce.args,
            template: zu_woocommerce.template,
        },
        beforeSend: function() {
            jQuery('.zu-woocommerce-body').addClass('loading');
            jQuery('#filter-result').css({'height': filterHeight});
        },
        success: function( result ) {
            jQuery('#filter-result').html(result.data);
        },
        complete: function () {
            jQuery('#filter-result').removeAttr('style');
            jQuery('#filter-result .slide-product').slick(zu_woocommerce.slick_config);
            setTimeout(function() {
                jQuery('.zu-woocommerce-body').removeClass('loading');
            }, 300);
        }
    });
};

<?php

$defaults = array(
    'title' => '',
    'content_align' => '',
    'icon' => '',
    'image' => '',
    'button_show' => '',
    'button_link' => '',
    'button_text' => '',
    'class' => '',
    'template' => '',
    'html_id' => '',
    'zu_template' => '',
);
$atts = shortcode_atts($defaults, $atts);
extract($atts);

//parse link
$link = ($button_link == '||') ? '' : $button_link;
$link = vc_build_link($link);
$use_link = false;
if (strlen($link['url']) > 0) {
    $use_link = true;
    $a_href = $link['url'];
    $a_title = $link['title'];
    $a_target = strlen($link['target']) > 0 ? $link['target'] : '_self';
}
if (!empty($image)) {
    $attachment_image = wp_get_attachment_image_src($image, 'full');
    $image = get_image_featured_uri($attachment_image[0], 350, 225);
}
?>

<div class="zu-service-box <?php echo esc_attr($template); ?>"
     id="<?php echo esc_attr($html_id); ?>">
    <?php if ($image): ?>
        <div class="box-image">
            <img src="<?php echo esc_attr($image); ?>"/>
        </div>
    <?php elseif ($icon): ?>
        <div class="box-icon">
            <i class="<?php echo esc_attr($icon); ?>"></i>
        </div>
    <?php endif; ?>
    <h2 class="box-title">
        <?php echo esc_attr($title); ?>
    </h2>
    <div class="box-description">
        <?php echo $content; ?>
    </div>
    <?php if ($button_show) : ?>
        <div class="box-button">
            <?php if ($use_link): ?>
                <a class="btn btn-primary btn-transparent""
                href="<?php echo esc_url($a_href); ?>"
                title="<?php echo esc_attr($a_title); ?>"
                target="<?php echo trim(esc_attr($a_target)); ?>"">
                <?php echo esc_attr($button_text); ?>
                </a>
            <?php else: ?>
                <button class="btn btn-primary btn-transparent">
                    <?php echo esc_attr($button_text); ?>
                </button>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>

<?php
/**
 * @var $this WPBakeryShortCode_VC_Btn
 * @var $atts
 * @var $style
 * @var $shape
 * @var $color
 * @var $custom_background
 * @var $custom_text
 * @var $size
 * @var $align
 * @var $link
 * @var $title
 * @var $button_block
 * @var $el_class
 * @var $inline_css
 * @var $button_type
 * @var $padding //Vu Edit
 * @var $border //Vu Edit
 * @var $style //Vu Edit
 * @var $letter_spacing //Vu Edit
 * @var $add_icon
 * @var $i_align
 * @var $i_type
 *
 * ///
 * @var $a_href
 * @var $a_title
 * @var $a_target
 */
$defaults = array(
    'button_type' => '',
    'padding' => '',
    'align' => 'inline',
    'link' => '',
    'title' => '',
    'letter_spacing' => '',
    'font_size' => '',
    'radius' => '',
    'color' => '',
    'button_block' => '',
    'el_class' => '',
    'add_icon' => '',
    'icon' => '',
    'i_align' => 'left',
    'css_animation' => '',
);
$icon_wrapper = false;
$icon_html = false;

$atts = shortcode_atts($defaults, $atts);
extract($atts);
//parse link
$link = ($link == '||') ? '' : $link;
$link = vc_build_link($link);
$use_link = false;
if (strlen($link['url']) > 0) {
    $use_link = true;
    $a_href = $link['url'];
    $a_title = $link['title'];
    $a_target = strlen($link['target']) > 0 ? $link['target'] : '_self';
}

$el_class = $this->getExtraClass($el_class);
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $el_class, $this->settings['base'], $atts);
$css_class .= $this->getCSSAnimation($css_animation);
$button_html = $title;
$button_class = '';
if ('' == trim($title)) {
    $button_class .= ' zu_btn-o-empty';
    $button_html = '<span class="zu_btn-placeholder">&nbsp;</span>';
}
if ('true' == $button_block && 'inline' != $align) {
    $button_class .= ' zu_btn-block';
}
if ('true' === $add_icon) {
    $button_class .= ' zu_btn-icon-' . $i_align;

    if ($icon_wrapper) {
        $icon_html = '<i class="zu_btn-icon"><span class="zu_btn-icon-inner ' . esc_attr($icon) . '"></span></i>';
    } else {
        $icon_html = '<i class="zu_btn-icon ' . esc_attr($icon) . '"></i>';
    }


    if ($i_align == 'left') {
        $button_html = $icon_html . ' ' . $button_html;
    } else {
        $button_html .= ' ' . $icon_html;
    }
}
$styleCss = array();
if (!empty($padding)) {
    $styleCss[] = 'padding:' . esc_attr($padding);
}
if (!empty($letter_spacing)) {
    $styleCss[] = 'letter-spacing:' . esc_attr($letter_spacing);
}
if (!empty($font_size)) {
    $styleCss[] = 'font-size:' . esc_attr($font_size);
}
if (!empty($radius)) {
    $styleCss[] = 'border-radius:' . esc_attr($radius);
}
if (!empty($color)) {
    $styleCss[] = 'color:' . esc_attr($color);
}
?>
<div class="zu-btn-wrap <?php echo esc_attr(trim($css_class)); ?> zu-btn-<?php echo esc_attr($align); ?>">
    <?php if ($use_link): ?>
        <a class="zu_btn <?php echo esc_attr($button_type); ?> <?php echo esc_attr(trim($button_class)); ?>"
           href="<?php echo esc_url($a_href); ?>"
           title="<?php echo esc_attr($a_title); ?>"
           target="<?php echo trim(esc_attr($a_target)); ?>"
           style="<?php echo implode(';', $styleCss); ?>">
            <?php echo $button_html; ?>
        </a>
    <?php else: ?>
        <button class="zu_btn <?php echo esc_attr($button_type); ?> <?php echo esc_attr($button_class); ?>"
                style="<?php echo implode(';', $styleCss); ?>">
            <?php echo $button_html; ?>
        </button>
    <?php endif; ?>
</div>

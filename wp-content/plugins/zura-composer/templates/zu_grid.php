<?php
/* get categories */
$taxonomy = 'category';
$_category = array();
if (!isset($atts['cat']) || $atts['cat'] == '') {
    $terms = get_terms($taxonomy);
    foreach ($terms as $cat) {
        $_category[] = $cat->term_id;
    }
} else {
    $_category = explode(',', $atts['cat']);
}
$atts['categories'] = $_category;
?>
<div class="zu-grid-wraper <?php echo esc_attr($atts['template']); ?>" id="<?php echo esc_attr($atts['html_id']); ?>">

    <?php if (isset($atts['filter']) && $atts['filter'] == 1 && $atts['layout'] == 'masonry'): ?>
        <div class="zu-grid-filter">
            <ul class="zu-filter-category list-unstyled list-inline">
                <li><a class="active" href="#" data-group="all"><?php esc_html_e("All", 'zura-composer'); ?></a></li>
                <?php
                $posts = $atts['posts'];
                $query = $posts->query;
                $taxs = array();
                if (isset($query['tax_query'])) {
                    $tax_query = $query['tax_query'];
                    foreach ($tax_query as $tax) {
                        if (is_array($tax)) {
                            $taxs[] = $tax['terms'];
                        }
                    }
                }
                foreach ($atts['categories'] as $category):
                    if (!empty($taxs)) {
                        if (in_array($category, $taxs)) {
                            $term = get_term($category, $taxonomy);
                            ?>
                            <li><a href="#"
                                   data-group="<?php echo esc_attr('category-' . $term->slug); ?>"><?php echo esc_attr($term->name); ?></a>
                            </li>
                        <?php }
                    } else {
                        $term = get_term($category, $taxonomy);
                        ?>
                        <li><a href="#"
                               data-group="<?php echo esc_attr('category-' . $term->slug); ?>"><?php echo esc_attr($term->name); ?></a>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
            </ul>
        </div>
    <?php endif; ?>

    <div class="row zu-grid <?php echo esc_attr($atts['grid_class']); ?>">
        <?php
        $posts = $atts['posts'];
        $size = (isset($atts['layout']) && $atts['layout'] == 'basic') ? 'thumbnail' : 'medium';
        while ($posts->have_posts()) {
            $posts->the_post();
            $groups = array();
            $groups[] = '"all"';
            foreach (zuGetCategoriesByPostID(get_the_ID(), $taxonomy) as $category) {
                $groups[] = '"category-' . $category->slug . '"';
            }
            ?>
            <div class="zu-grid-item <?php echo esc_attr($atts['item_class']); ?>"
                 data-groups='[<?php echo implode(',', $groups); ?>]'>
                <?php
                if (has_post_thumbnail() && !post_password_required() && !is_attachment() && wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), $size, false)):
                    $class = ' has-thumbnail';
                    $thumbnail = get_the_post_thumbnail(get_the_ID(), $size);
                else:
                    $class = ' no-image';
                    $thumbnail = '<img src="' . ZU_IMAGES . 'no-image.jpg" alt="' . get_the_title() . '" />';
                endif;
                echo '<div class="zu-grid-media ' . esc_attr($class) . '">' . $thumbnail . '</div>';
                ?>
                <div class="zu-grid-title">
                    <?php the_title(); ?>
                </div>
                <div class="zu-grid-time">
                    <?php the_time('l, F jS, Y'); ?>
                </div>
                <div class="zu-grid-categories">
                    <?php echo get_the_term_list(get_the_ID(), $taxonomy, 'Category: ', ', ', ''); ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php wp_reset_postdata(); ?>
</div>

<div class="zu-woocommerce-wraper zu-woocommerce-layout-style-02 <?php echo esc_attr($atts['zu_template']); ?>"
     id="<?php echo esc_attr($atts['html_id']); ?>">

    <?php if($atts['woo_filter'] != 0) : ?>
    <div class="zu-woocommerce-head">
        <ul class="list-cat-filter">
            <?php
            foreach( $atts['term'] as $term ) {
                $classActive = $atts['term_id'] == $term->term_id ? 'class="active"' : '';
                echo '<li class="cat-item term-id-'.$term->term_id.'"><a href="#" data-term-id="'.$term->term_id.'" '.$classActive.'>'.$term->name.'</a></li>';
            }
            ?>
        </ul>
    </div>
    <?php endif; ?>

    <div class="row zu-woocommerce-body">
        <div class="zu-lds-ellipsis" id="zu-loading"><div></div><div></div><div></div><div></div></div>
        <div id="filter-result">
            <?php
                if($atts['products']) {
                    $products = $atts['products'];
                    if($products->have_posts()) {
                        echo '<div class="slide-product">';
                        while ( $products->have_posts() ) : $products->the_post();
                            get_template_part( 'views/woocommerce/product--style-2' );
                        endwhile;
                        echo '</div>';
                    }
                }
            ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
</div>
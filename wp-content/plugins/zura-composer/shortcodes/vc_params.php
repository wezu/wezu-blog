<?php
vc_add_params('vc_row', array(
    array(
        'type' => 'checkbox',
        'heading' => 'Full Width',
        'param_name' => 'full_row',
        'value' => '',
        'weight' => 1,
        'description' => esc_html__('Enable row container.', 'zura-composer')
    ),
    array(
        'type' => 'checkbox',
        'heading' => 'Columns no gap',
        'param_name' => 'columns_no_gap',
        'value' => '',
        'weight' => 1,
        'description' => esc_html__('Enable no gap between columns in row.', 'zura-composer')
    )
));

vc_remove_param("vc_row", "full_width");
vc_remove_param("vc_row", "gap");

//custom header
vc_add_param("vc_custom_heading", array(
    "type" => "dropdown",
    "heading" => esc_html__("Custom Heading Style", 'zura-composer'),
    "admin_label" => true,
    "param_name" => "heading_style",
    "value" => array(
        "Default" => 'default',
        "Title Underline - Center" => "underline-center",
        "Title Underline - Full" => "underline-full"
    )
));
vc_add_param("vc_custom_heading", array(
    "type" => "textfield",
    "heading" => esc_html__("Subtitle", 'zura-composer'),
    "admin_label" => true,
    "param_name" => "subtitle",
    "value" => ""
));
vc_add_param("vc_custom_heading", array(
    'type' => 'checkbox',
    'heading' => esc_html__('Add icon?', 'zura-composer'),
    'param_name' => 'add_icon',
));

vc_add_param("vc_custom_heading", array(
    'type' => 'iconpicker',
    'heading' => __('Icon', 'zura-composer'),
    'param_name' => 'icon',
    'value' => '',
    'settings' => array(
        'emptyIcon' => true, // default true, display an "EMPTY" icon?
        'iconsPerPage' => 200, // default 100, how many icons per/page to display
    ),
    'dependency' => array(
        'element' => 'add_icon',
        'value' => 'true',
    ),
    'description' => __('Select icon from library.', 'zura-composer'),
));

vc_add_param("vc_custom_heading", array(
    "type" => "attach_image",
    "heading" => __("Icon Image", 'zura-composer'),
    "param_name" => "image",
    'dependency' => array(
        'element' => 'add_icon',
        'value' => 'true',
    )
));

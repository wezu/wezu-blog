<?php
vc_map(
    array(
        "name" => __("ZU Service Box", 'zura-composer'),
        "base" => "zu_service",
        "class" => "vc-zu-service-box",
        "category" => __("Zura Shortcodes", 'zura-composer'),
        "params" => array(
            array(
                "type" => "textfield",
                "heading" => __("Title", 'zura-composer'),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title of service box", 'zura-composer'),
            ),
            array(
                "type" => "textarea_html",
                "heading" => __("Content", 'zura-composer'),
                "param_name" => "content",
                "value" => "",
                "description" => __("Content of service box", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Content Align", 'zura-composer'),
                "param_name" => "content_align",
                "value" => array(
                    "Default" => "default",
                    "Left" => "left",
                    "Right" => "right",
                    "Center" => "center",
                ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Icon', 'zura-composer'),
                'param_name' => 'icon',
                'value' => '',
                'settings' => array(
                    'emptyIcon' => true, // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 200, // default 100, how many icons per/page to display
                ),
                'description' => __('Select icon from library.', 'zura-composer'),
            ),
            array(
                "type" => "attach_image",
                "heading" => __("Image", 'zura-composer'),
                "param_name" => "image",
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_html__('Show button?', 'zura-composer'),
                'param_name' => 'button_show',
            ),
            array(
                'type' => 'vc_link',
                'heading' => esc_html__('URL (Link)', 'zura-composer'),
                'param_name' => 'button_link',
                'description' => esc_html__('Add link to button.', 'zura-composer'),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Text Button', 'zura-composer'),
                'save_always' => true,
                'param_name' => 'button_text',
                'value' => esc_html__('Read more', 'zura-composer'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Extra Class", 'zura-composer'),
                "param_name" => "class",
                "description" => __("", 'zura-composer'),
                "group" => __("Template", 'zura-composer'),
            ),
            array(
                "type" => "zu_template",
                "param_name" => "zu_template",
                "admin_label" => true,
                "heading" => __("Shortcode Template", 'zura-composer'),
                "shortcode" => "zu_service",
                "group" => __("Template", 'zura-composer'),
            ),
        ),
    )
);

class WPBakeryShortCode_zu_service extends ZuShortcode
{
    protected function content($atts, $content = null)
    {
        $atts_extra = shortcode_atts(array(
            'title' => '',
            'content_align' => 'default',
            'zu_cols' => '1 Column',
            'button_type' => 'button',
            'button_text' => '',
            'class' => '',
            'zu_template' => 'zu_service.php',
        ), $atts);
        $atts = array_merge($atts_extra, $atts);
        $html_id = zuHtmlID('zu-service-box');

        $atts['template'] = 'template-' . str_replace('.php', '', $atts['zu_template']) . $atts['class'];
        $atts['html_id'] = $html_id;
        return parent::content($atts, $content);
    }
}

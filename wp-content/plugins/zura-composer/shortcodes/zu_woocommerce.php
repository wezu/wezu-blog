<?php
vc_map(
    array(
        "name" => __("ZU WooCommerce", 'zura-composer'),
        "base" => "zu_woocommerce",
        "class" => "vc-zu-woocommerce",
        "category" => __("Zura Shortcodes", 'zura-composer'),
        "params" => array(
            array(
                "type" => "loop",
                "heading" => __("Source", 'zura-composer'),
                "param_name" => "source",
                'settings' => array(
                    'size' => array('hidden' => 0, 'value' => 10),
                    'order_by' => array('value' => 'date'),
                ),
                "group" => __("Source Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Rows - Small Devices", 'zura-composer'),
                "param_name" => "rows_small_items",
                "edit_field_class" => "vc_col-sm-4 vc_slick_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Rows - Medium Devices", 'zura-composer'),
                "param_name" => "rows_medium_items",
                "edit_field_class" => "vc_col-sm-4 vc_slick_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Rows - Large Devices", 'zura-composer'),
                "param_name" => "rows_large_items",
                "edit_field_class" => "vc_col-sm-4 vc_slick_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("SlidesPerRow - Small Devices", 'zura-composer'),
                "param_name" => "slides_per_row_small_items",
                "edit_field_class" => "vc_col-sm-4 vc_slick_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("SlidesPerRow - Medium Devices", 'zura-composer'),
                "param_name" => "slides_per_row_medium_items",
                "edit_field_class" => "vc_col-sm-4 vc_slick_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("SlidesPerRow - Large Devices", 'zura-composer'),
                "param_name" => "slides_per_row_large_items",
                "edit_field_class" => "vc_col-sm-4 vc_slick_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Dots", 'zura-composer'),
                "param_name" => "slick_dots",
                "value" => array(
                    "Yes" => 1,
                    "No" => 0,
                ),
                'std' => 0,
                'description' => __('', 'zura-composer'),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Variable Width", 'zura-composer'),
                "param_name" => "variable_width",
                "value" => array(
                    "Yes" => 1,
                    "No" => 0,
                ),
                'std' => 0,
                'description' => __('', 'zura-composer'),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Filter", 'zura-composer'),
                "param_name" => "woo_filter",
                "value" => array(
                    "Disable" => 0,
                    "Enable" => 1,
                ),
                "std" => 1,
                'description' => __('The filter don\'t work with Loop.', 'zura-composer'),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Left Arrow', 'zura-composer'),
                'param_name' => 'left_arrow',
                'value' => 'fa fa-arrow-left',
                'settings' => array(
                    'emptyIcon' => 0, // default 1, display an "EMPTY" icon?
                    'iconsPerPage' => 200, // default 100, how many icons per/page to display
                ),
                'description' => __('Select icon from library.', 'zura-composer'),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Right Arrow', 'zura-composer'),
                'param_name' => 'right_arrow',
                'value' => 'fa fa-arrow-right',
                'settings' => array(
                    'emptyIcon' => 0, // default 1, display an "EMPTY" icon?
                    'iconsPerPage' => 200, // default 100, how many icons per/page to display
                ),
                'description' => __('Select icon from library.', 'zura-composer'),
                "group" => __("Slick Settings", 'zura-composer'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Extra Class", 'zura-composer'),
                "param_name" => "class",
                "value" => "",
                "description" => __("", 'zura-composer'),
                "group" => __("Template", 'zura-composer'),
            ),
            array(
                "type" => "zu_template",
                "param_name" => "zu_template",
                "shortcode" => "zu_woocommerce",
                "admin_label" => true,
                "heading" => __("Shortcode Template", 'zura-composer'),
                "group" => __("Template", 'zura-composer'),
            ),
        ),
    )
);

class WPBakeryShortCode_zu_woocommerce extends ZuShortcode
{
    protected function content($atts, $content = null)
    {
        $source = $atts['source'];
        list( $args, $products ) = vc_build_loop_query( $source );
        $atts['products'] = $products;
        $taxonomy = $args['post_type'][0] === 'product' ? 'product_cat' : 'category';

        $terms = get_terms($taxonomy);
        $atts['term'] = $terms;
        foreach( $terms as $term ) {
            $atts['term_id'] = $term->term_id;
            break;
        }
        $template = $atts['zu_template'] == 'zu_woocommerce.php' ? 'product' : 'product'.substr($atts['zu_template'], 14, 9);
        $atts['woo_filter'] = isset($atts['woo_filter']) ? $atts['woo_filter'] : 1;
        $left_arrow = isset($atts['left_arrow']) ? $atts['left_arrow'] : 'fa fa-arrow-left';
        $right_arrow = isset($atts['right_arrow']) ? $atts['right_arrow'] : 'fa fa-arrow-right';
        $atts['item_class'] = 'zu-col-lg-'.$atts['slides_per_row_large_items'].'zu-col-md-'.$atts['slides_per_row_medium_items'].'zu-col-xs-'.$atts['slides_per_row_small_items'];
        $atts['variable_width'] = isset($atts['variable_width']) ? ($atts['variable_width'] == 1 ? true : false )  : false;
        $slick_config = array(
            'fade' => true,
            'dots' => $atts['slick_dots'] == 1 ? true : false,
            'slidesPerRow' => $atts['slides_per_row_large_items'],
            'rows' => $atts['rows_large_items'],
            'prevArrow' => '<div data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button"><i class="' . $left_arrow . '"></i></div>',
            'nextArrow' => '<div data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button"><i class="' . $right_arrow . '"></i></div>',
            'variableWidth' => $atts['variable_width'],
            'responsive' => array(
                array(
                    'breakpoint' => 992,
                    'settings' => array(
                        'slidesPerRow' => $atts['slides_per_row_medium_items'],
                        'rows' => $atts['rows_medium_items'],
                        'arrow' => false,
                        'dots' => true,
                    ),

                ),
                array(
                    'breakpoint' => 641,
                    'settings' => array(
                        'slidesPerRow' => $atts['slides_per_row_small_items'],
                        'rows' => $atts['rows_small_items'],
                        'arrow' => false,
                        'dots' => true,
                    ),

                ),
            )
        );

        wp_enqueue_style('slick-css', ZU_CSS . 'slick.css');
        wp_enqueue_script('slick-js', ZU_JS . 'slick.min.js', true);
        wp_enqueue_script('zu_woocommerce-js', ZU_JS . 'zu.woocommerce.js', array('jquery'), '', true);
        if($atts['woo_filter'] != 0) {
            wp_localize_script('zu_woocommerce-js', 'zu_woocommerce', array(
                'ajax_url' => admin_url('admin-ajax.php'),
                'args' => $args,
                'template' => $template,
                'slick_config' => $slick_config
            ));
        } else {
            wp_localize_script('zu_woocommerce-js', 'zu_woocommerce', array(
                'slick_config' => $slick_config
            ));
        }

        // Remove the comment form
        add_filter( 'comments_open', '__return_false' );

        // Remove the list of comments
        add_filter( 'comments_array', '__return_empty_array' );

        $atts['html_id'] = zuHtmlID('zu-woocommerce');

        return parent::content($atts, $content);
    }
}
if(!function_exists( 'zu_woocommerce' )) {
    function zu_woocommerce() {
        $result = '';
        $term_id = $_POST['term_id'];
        $args = $_POST['args'];
        $taxonomy = $args['post_type'][0] === 'product' ? 'product_cat' : 'category';
        $template_path = 'views/woocommerce/'.$_POST['template'];
        $args = array(
            'post_type' => $args['post_type'][0],
            'post_status' => $args['post_status'],
            'posts_per_page' => $args['posts_per_page'],
            'showposts' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => $taxonomy,
                    'field' => 'term_id',
                    'terms' => $term_id
                )
            )

        );
        $products = new WP_Query( $args );

        if($products->have_posts()) {
            ob_start();
            echo '<div class="slide-product">';
            while ( $products->have_posts() ) : $products->the_post();
                get_template_part( $template_path );
            endwhile;
            echo '</div>';
            $result = ob_get_clean();
        }

        wp_send_json_success( $result );
        die();
    }
}

add_action( 'wp_ajax_zu_woocommerce', 'zu_woocommerce' );
add_action( 'wp_ajax_nopriv_zu_woocommerce', 'zu_woocommerce' );
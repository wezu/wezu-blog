<?php
vc_map(
    array(
        "name" => __("ZU Grid", 'zura-composer'),
        "base" => "zu_grid",
        "class" => "vc-zu-grid",
        "category" => __("Zura Shortcodes", 'zura-composer'),
        "params" => array(
            array(
                "type" => "loop",
                "heading" => __("Source", 'zura-composer'),
                "param_name" => "source",
                'settings' => array(
                    'size' => array('hidden' => false, 'value' => 10),
                    'order_by' => array('value' => 'date'),
                ),
                "group" => __("Source Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Layout Type", 'zura-composer'),
                "param_name" => "layout",
                "value" => array(
                    "Basic" => "basic",
                    "Masonry" => "masonry",
                ),
                "group" => __("Grid Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Columns XS Devices", 'zura-composer'),
                "param_name" => "col_xs",
                "edit_field_class" => "vc_col-sm-3 vc_column",
                "value" => array(1, 2, 3, 4, 5, 6, 12),
                "std" => 1,
                "group" => __("Grid Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Columns SM Devices", 'zura-composer'),
                "param_name" => "col_sm",
                "edit_field_class" => "vc_col-sm-3 vc_column",
                "value" => array(1, 2, 3, 4, 5, 6, 12),
                "std" => 2,
                "group" => __("Grid Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Columns MD Devices", 'zura-composer'),
                "param_name" => "col_md",
                "edit_field_class" => "vc_col-sm-3 vc_column",
                "value" => array(1, 2, 3, 4, 5, 6, 12),
                "std" => 3,
                "group" => __("Grid Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Columns LG Devices", 'zura-composer'),
                "param_name" => "col_lg",
                "edit_field_class" => "vc_col-sm-3 vc_column",
                "value" => array(1, 2, 3, 4, 5, 6, 12),
                "std" => 4,
                "group" => __("Grid Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Filter on Masonry", 'zura-composer'),
                "param_name" => "filter",
                "value" => array(
                    "Disable" => 0,
                    "Enable" => 1,
                ),
                "dependency" => array(
                    "element" => "layout",
                    "value" => "masonry",
                ),
                "group" => __("Grid Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Enable Animation", 'zura-composer'),
                "param_name" => "enable_animation",
                "value" => array(
                    "Disable" => 0,
                    "Enable" => 1,
                ),
                "std" => 0,
                "group" => __("Grid Settings", 'zura-composer'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Extra Class", 'zura-composer'),
                "param_name" => "class",
                "value" => "",
                "description" => __("", 'zura-composer'),
                "group" => __("Template", 'zura-composer'),
            ),
            array(
                "type" => "zu_template",
                "param_name" => "zu_template",
                "shortcode" => "zu_grid",
                "admin_label" => true,
                "heading" => __("Shortcode Template", 'zura-composer'),
                "group" => __("Template", 'zura-composer'),
            ),
        ),
    )
);

class WPBakeryShortCode_zu_grid extends ZuShortcode
{
    protected function content($atts, $content = null)
    {
        wp_enqueue_script('zu-grid-pagination', ZU_JS . 'zugrid.pagination.js', array('jquery'), '1.0.0', true);
        $html_id = zuHtmlID('zu-grid');
        $source = $atts['source'];
        list($args, $wp_query) = vc_build_loop_query($source);

        $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;

        if ($paged > 1) {
            $args['paged'] = $paged;
            $wp_query = new WP_Query($args);
        }
        $atts['cat'] = isset($args['cat']) ? $args['cat'] : '';
        /* get posts */
        $atts['posts'] = $wp_query;
        $grid = shortcode_atts(array(
            'col_lg' => 4,
            'col_md' => 3,
            'col_sm' => 2,
            'col_xs' => 1,
            'layout' => 'basic',
            'enable_animation' => 0,
            'zu_template' => 'zu_grid.php',
        ), $atts);
        $col_lg = $grid['col_lg'] == 5 ? '2-zu' : 12 / $grid['col_lg'];
        $col_md = $grid['col_md'] == 5 ? '2-zu' : 12 / $grid['col_md'];
        $col_sm = $grid['col_sm'] == 5 ? '2-zu' : 12 / $grid['col_sm'];
        $col_xs = $grid['col_xs'] == 5 ? '2-zu' : 12 / $grid['col_xs'];
        $atts['item_class'] = "zu-grid-item col-lg-{$col_lg} col-md-{$col_md} col-sm-{$col_sm} col-xs-{$col_xs}";
        $atts['grid_class'] = "zu-grid";
        $class = isset($atts['class']) ? $atts['class'] : '';
        $atts['template'] = 'template-' . str_replace('.php', '', $atts['zu_template']) . ' ' . $class;
        if ($grid['layout'] == 'masonry') {
            wp_enqueue_script('zu-jquery-shuffle');
            $atts['grid_class'] .= " zu-grid-{$grid['layout']}";
        }
        $atts['html_id'] = $html_id;
        $atts['enable_animation'] = $grid['enable_animation'];

        if ($atts['enable_animation']) {
            wp_enqueue_script('waypoints');
            wp_enqueue_style('animate-css');
        }

        return parent::content($atts, $content);
    }
}

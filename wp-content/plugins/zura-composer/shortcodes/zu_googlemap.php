<?php
vc_map(array(
    "name" => 'ZU Google Map',
    "base" => "zu_googlemap",
    "icon" => "zu_icon_for_vc",
    "category" => __('Zura Shortcodes', 'zura-composer'),
    "description" => __('Map API V3 Unlimited Style', 'zura-composer'),
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => __('API Key', 'zura-composer'),
            "param_name" => "api",
            "value" => '',
            "description" => __('Enter you api key of map, get key from (https://console.developers.google.com)', 'zura-composer'),
        ),
        array(
            "type" => "textfield",
            "heading" => __('Address', 'zura-composer'),
            "param_name" => "address",
            "value" => 'New York, United States',
            "description" => __('Enter address of Map', 'zura-composer'),
        ),
        array(
            "type" => "textfield",
            "heading" => __('Coordinate', 'zura-composer'),
            "param_name" => "coordinate",
            "value" => '',
            "description" => __('Enter coordinate of Map, format input (latitude, longitude)', 'zura-composer'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Click Show Info window', 'zura-composer'),
            "param_name" => "infoclick",
            "value" => array(
                __("Yes, please", 'zura-composer') => true,
            ),
            "description" => __('Click a marker and show info window (Default Show).', 'zura-composer'),
        ),
        array(
            "type" => "textfield",
            "heading" => __('Marker Coordinate', 'zura-composer'),
            "param_name" => "markercoordinate",
            "value" => '',
            "description" => __('Enter marker coordinate of Map, format input (latitude, longitude)', 'zura-composer'),
        ),
        array(
            "type" => "textfield",
            "heading" => __('Marker Title', 'zura-composer'),
            "param_name" => "markertitle",
            "value" => '',
            "description" => __('Enter Title Info windows for marker', 'zura-composer'),
        ),
        array(
            "type" => "textarea",
            "heading" => __('Marker Description', 'zura-composer'),
            "param_name" => "markerdesc",
            "value" => '',
            "description" => __('Enter Description Info windows for marker', 'zura-composer'),
        ),
        array(
            "type" => "attach_image",
            "heading" => __('Marker Icon', 'zura-composer'),
            "param_name" => "markericon",
            "value" => '',
            "description" => __('Select image icon for marker', 'zura-composer'),
        ),
        array(
            "type" => "textarea_raw_html",
            "heading" => __('Marker List', 'zura-composer'),
            "param_name" => "markerlist",
            "value" => '',
            "description" => __('[{"coordinate":"41.058846,-73.539423","icon":"","title":"title demo 1","desc":"desc demo 1"},{"coordinate":"40.975699,-73.717636","icon":"","title":"title demo 2","desc":"desc demo 2"},{"coordinate":"41.082606,-73.469718","icon":"","title":"title demo 3","desc":"desc demo 3"}]', 'zura-composer'),
        ),
        array(
            "type" => "textfield",
            "heading" => __('Info Window Max Width', 'zura-composer'),
            "param_name" => "infowidth",
            "value" => '200',
            "description" => __('Set max width for info window', 'zura-composer'),
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Map Type", 'zura-composer'),
            "param_name" => "type",
            "value" => array(
                "ROADMAP" => "ROADMAP",
                "HYBRID" => "HYBRID",
                "SATELLITE" => "SATELLITE",
                "TERRAIN" => "TERRAIN",
            ),
            "description" => __('Select the map type.', 'zura-composer'),
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Style Template", 'zura-composer'),
            "param_name" => "style",
            "value" => array(
                "Default" => "",
                "Custom" => "custom",
                "Light Monochrome" => "light-monochrome",
                "Blue water" => "blue-water",
                "Midnight Commander" => "midnight-commander",
                "Paper" => "paper",
                "Red Hues" => "red-hues",
                "Hot Pink" => "hot-pink",
            ),
            "dependency" => array(
                "element" => "type",
                "value" => "ROADMAP",
            ),
            "description" => 'Select your heading size for title.',
        ),
        array(
            "type" => "textarea",
            "heading" => __('Custom Template', 'zura-composer'),
            "param_name" => "content",
            "value" => '',
            "dependency" => array(
                "element" => "style",
                "value" => "custom",
            ),
            "description" => __('Get template from http://snazzymaps.com', 'zura-composer'),
        ),
        array(
            "type" => "textfield",
            "heading" => __('Zuom', 'zura-composer'),
            "param_name" => "zuom",
            "value" => '13',
            "description" => __('zuom level of map, default is 13', 'zura-composer'),
        ),
        array(
            "type" => "textfield",
            "heading" => __('Width', 'zura-composer'),
            "param_name" => "width",
            "value" => 'auto',
            "description" => __('Width of map without pixel, default is auto', 'zura-composer'),
        ),
        array(
            "type" => "textfield",
            "heading" => __('Height', 'zura-composer'),
            "param_name" => "height",
            "value" => '350px',
            "description" => __('Height of map without pixel, default is 350px', 'zura-composer'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Scroll Wheel', 'zura-composer'),
            "param_name" => "scrollwheel",
            "value" => array(
                __("Yes, please", 'zura-composer') => true,
            ),
            "description" => __('If false, disables scrollwheel zuoming on the map. The scrollwheel is disable by default.', 'zura-composer'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Pan Control', 'zura-composer'),
            "param_name" => "pancontrol",
            "value" => array(
                __("Yes, please", 'zura-composer') => true,
            ),
            "description" => __('Show or hide Pan control.', 'zura-composer'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Zuom Control', 'zura-composer'),
            "param_name" => "zuomcontrol",
            "value" => array(
                __("Yes, please", 'zura-composer') => true,
            ),
            "description" => __('Show or hide Zuom Control.', 'zura-composer'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Scale Control', 'zura-composer'),
            "param_name" => "scalecontrol",
            "value" => array(
                __("Yes, please", 'zura-composer') => true,
            ),
            "description" => __('Show or hide Scale Control.', 'zura-composer'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Map Type Control', 'zura-composer'),
            "param_name" => "maptypecontrol",
            "value" => array(
                __("Yes, please", 'zura-composer') => true,
            ),
            "description" => __('Show or hide Map Type Control.', 'zura-composer'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Street View Control', 'zura-composer'),
            "param_name" => "streetviewcontrol",
            "value" => array(
                __("Yes, please", 'zura-composer') => true,
            ),
            "description" => __('Show or hide Street View Control.', 'zura-composer'),
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Over View Map Control', 'zura-composer'),
            "param_name" => "overviewmapcontrol",
            "value" => array(
                __("Yes, please", 'zura-composer') => true,
            ),
            "description" => __('Show or hide Over View Map Control.', 'zura-composer'),
        ),
    ),
));

class WPBakeryShortCode_zu_googlemap extends ZuShortcode
{

    protected function content($attrs, $content = null)
    {
        return parent::content($attrs, $content);
    }
}

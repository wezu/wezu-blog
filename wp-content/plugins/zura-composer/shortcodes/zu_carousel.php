<?php
vc_map(
    array(
        "name" => __("ZU Carousel", 'zura-composer'),
        "base" => "zu_carousel",
        "class" => "vc-zu-carousel",
        "category" => __("Zura Shortcodes", 'zura-composer'),
        "params" => array(
            array(
                "type" => "loop",
                "heading" => __("Source", 'zura-composer'),
                "param_name" => "source",
                'settings' => array(
                    'size' => array('hidden' => 0, 'value' => 10),
                    'order_by' => array('value' => 'date'),
                ),
                "group" => __("Source Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("XSmall Devices", 'zura-composer'),
                "param_name" => "xsmall_items",
                "edit_field_class" => "vc_col-sm-3 vc_carousel_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "std" => 1,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Small Devices", 'zura-composer'),
                "param_name" => "small_items",
                "edit_field_class" => "vc_col-sm-3 vc_carousel_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "std" => 2,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Medium Devices", 'zura-composer'),
                "param_name" => "medium_items",
                "edit_field_class" => "vc_col-sm-3 vc_carousel_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "std" => 3,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Large Devices", 'zura-composer'),
                "param_name" => "large_items",
                "edit_field_class" => "vc_col-sm-3 vc_carousel_item",
                "value" => array(1, 2, 3, 4, 5, 6),
                "std" => 4,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Margin Items", 'zura-composer'),
                "param_name" => "margin",
                "value" => "10",
                "description" => __("", 'zura-composer'),
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Loop Items", 'zura-composer'),
                "param_name" => "loop",
                "value" => array(
                    "True" => 1,
                    "False" => 0,
                ),
                'std' => 1,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Mouse Drag", 'zura-composer'),
                "param_name" => "mousedrag",
                "value" => array(
                    "True" => 1,
                    "False" => 0,
                ),
                'std' => 1,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Touch Drag", 'zura-composer'),
                "param_name" => "touchdrag",
                "value" => array(
                    "True" => 1,
                    "False" => 0,
                ),
                'std' => 1,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Show Nav", 'zura-composer'),
                "param_name" => "nav",
                "value" => array(
                    "True" => 1,
                    "False" => 0,
                ),
                'std' => 1,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Show Dots", 'zura-composer'),
                "param_name" => "dots",
                "value" => array(
                    "True" => 1,
                    "False" => 0,
                ),
                'std' => 1,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Center", 'zura-composer'),
                "param_name" => "center",
                "value" => array(
                    "False" => 0,
                    "True" => 1,
                ),
                'std' => 0,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Auto Play", 'zura-composer'),
                "param_name" => "autoplay",
                "value" => array(
                    "True" => 1,
                    "False" => 0,
                ),
                'std' => 1,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Auto Play TimeOut", 'zura-composer'),
                "param_name" => "autoplaytimeout",
                "value" => "5000",
                "dependency" => array(
                    "element" => "autoplay",
                    "value" => 1,
                ),
                "description" => __("", 'zura-composer'),
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Smart Speed", 'zura-composer'),
                "param_name" => "smartspeed",
                "value" => "1000",
                "description" => __("Speed scroll of each item", 'zura-composer'),
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Pause On Hover", 'zura-composer'),
                "param_name" => "autoplayhoverpause",
                "dependency" => array(
                    "element" => "autoplay",
                    "value" => 1,
                ),
                "value" => array(
                    "True" => 1,
                    "False" => 0,
                ),
                'std' => 1,
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Left Arrow', 'zura-composer'),
                'param_name' => 'left_arrow',
                'value' => 'fa fa-arrow-left',
                'settings' => array(
                    'emptyIcon' => 0, // default 1, display an "EMPTY" icon?
                    'iconsPerPage' => 200, // default 100, how many icons per/page to display
                ),
                'description' => __('Select icon from library.', 'zura-composer'),
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Right Arrow', 'zura-composer'),
                'param_name' => 'right_arrow',
                'value' => 'fa fa-arrow-right',
                'settings' => array(
                    'emptyIcon' => 0, // default 1, display an "EMPTY" icon?
                    'iconsPerPage' => 200, // default 100, how many icons per/page to display
                ),
                'description' => __('Select icon from library.', 'zura-composer'),
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Filter", 'zura-composer'),
                "param_name" => "filter",
                "value" => array(
                    "Disable" => "false",
                    "Enable" => "true",
                ),
                'std' => "false",
                'description' => __('The filter don\'t work with Loop.', 'zura-composer'),
                "group" => __("Carousel Settings", 'zura-composer'),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Extra Class", 'zura-composer'),
                "param_name" => "class",
                "value" => "",
                "description" => __("", 'zura-composer'),
                "group" => __("Template", 'zura-composer'),
            ),
            array(
                "type" => "zu_template",
                "param_name" => "zu_template",
                "shortcode" => "zu_carousel",
                "admin_label" => true,
                "heading" => __("Shortcode Template", 'zura-composer'),
                "group" => __("Template", 'zura-composer'),
            ),
        ),
    )
);
global $zu_carousel;
$zu_carousel = array();

class WPBakeryShortCode_zu_carousel extends ZuShortcode
{
    protected function content($atts, $content = null)
    {
        //default value
        $atts_extra = shortcode_atts(array(
            'xsmall_items' => 1,
            'small_items' => 2,
            'medium_items' => 3,
            'large_items' => 4,
            'margin' => 0,
            'loop' => 1,
            'mousedrag' => 1,
            'touchdrag' => 1,
            'nav' => 1,
            'dots' => 1,
            'center' => 0,
            'autoplay' => 1,
            'autoplaytimeout' => '5000',
            'smartspeed' => '250',
            'autoplayhoverpause' => 1,
            'left_arrow' => 'fa fa-arrow-left',
            'right_arrow' => 'fa fa-arrow-right',
            'filter' => "false",
            'class' => '',
            'zu_template' => 'zu_carousel.php',
        ), $atts);
        global $zu_carousel;
        $atts = array_merge($atts_extra, $atts);
        wp_enqueue_style('owl-carousel', ZU_CSS . 'owl.carousel.min.css', '', '2.3.4', 'all');
        wp_enqueue_script('owl-carousel', ZU_JS . 'owl.carousel.min.js', array('jquery'), '2.3.4', true);

        wp_enqueue_script('owl-carousel-zu', ZU_JS . 'owl.carousel.zu.js', array('jquery'), '1.0.0', true);

        $source = $atts['source'];
        list($args, $posts) = vc_build_loop_query($source);
        $atts['posts'] = $posts;
        $html_id = zuHtmlID('zu-carousel');
        $atts['autoplaytimeout'] = isset($atts['autoplaytimeout']) ? (int)$atts['autoplaytimeout'] : 5000;
        $atts['smartspeed'] = isset($atts['smartspeed']) ? (int)$atts['smartspeed'] : 250;
        $left_arrow = isset($atts['left_arrow']) ? $atts['left_arrow'] : 'fa fa-arrow-left';
        $right_arrow = isset($atts['right_arrow']) ? $atts['right_arrow'] : 'fa fa-arrow-right';

        $zu_carousel[$html_id] = array(
            'margin' => (int)$atts['margin'],
            'loop' => $atts['loop'] == 1 ? true : false,
            'mouseDrag' => $atts['mousedrag'] == 1 ? true : false,
            'touchDrag' => $atts['touchdrag'] == 1 ? true : false,
            'nav' => $atts['nav'] == 1 ? true : false,
            'dots' => $atts['dots'] == 1 ? true : false,
            'center' => $atts['center'] == 1 ? true : false,
            'autoplay' => $atts['autoplay'] == 1 ? true : false,
            'autoplayTimeout' => $atts['autoplaytimeout'],
            'smartSpeed' => $atts['smartspeed'],
            'autoplayHoverPause' => $atts['autoplayhoverpause'] == 1 ? true : false,
            'navText' => array('<i class="' . $left_arrow . '"></i>', '<i class="' . $right_arrow . '"></i>'),
            'dotscontainer' => $html_id . ' .zu-dots',
            'items' => (int)$atts['large_items'],
            'responsive' => array(
                0 => array(
                    "items" => (int)$atts['xsmall_items'],
                ),
                768 => array(
                    "items" => (int)$atts['small_items'],
                ),
                992 => array(
                    "items" => (int)$atts['medium_items'],
                ),
                1200 => array(
                    "items" => (int)$atts['large_items'],
                ),
            ),
        );
        wp_localize_script('owl-carousel-zu', "zuCarousel", $zu_carousel);
        $atts['template'] = 'template-' . str_replace('.php', '', $atts['zu_template']) . ' ' . $atts['class'];
        $atts['html_id'] = $html_id;

        return parent::content($atts, $content);
    }
}
<?php
/**
 * New button implementation
 * array_merge is needed due to merging other shortcode data into params.
 * @since 4.5
 */
$params = array_merge(
    array(
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Text Button', 'zura-composer'),
            'save_always' => true,
            'param_name' => 'title',
            'value' => esc_html__('Text on the button', 'zura-composer'),
        ),
        array(
            'type' => 'vc_link',
            'heading' => esc_html__('URL (Link)', 'zura-composer'),
            'param_name' => 'link',
            'description' => esc_html__('Add link to button.', 'zura-composer'),
        ),
        array(
            "type" => "dropdown",
            "class" => "",
            "heading" => esc_html__("Button Type", 'zura-composer'),
            "param_name" => "button_type",
            "value" => array(
                'Button Default' => 'btn btn-default',
                'Button Primary' => 'btn btn-primary',
                'Button Secondary' => 'btn btn-secondary',
                'Button Info' => 'btn btn-info',
                'Button Warning' => 'btn btn-warning',
                'Button Success' => 'btn btn-success',
                'Button Danger' => 'btn btn-danger',
                'Button Primary Transparent' => 'btn btn-primary btn-transparent',
            )
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Custom style?', 'zura-composer'),
            'param_name' => 'custom_style',
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Padding', 'zura-composer'),
            'param_name' => 'padding',
            'description' => esc_html__('Ex: 15px 35px 15px 35px.', 'zura-composer'),
            'dependency' => array(
                'element' => 'custom_style',
                'value' => 'true'
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Letter Spacing', 'zura-composer'),
            'param_name' => 'letter_spacing',
            'value' => array(
                esc_html__('Default', 'zura-composer') => '',
                esc_html__('Letter Spacing: 50', 'zura-composer') => '0.05em',
                esc_html__('Letter Spacing: 100', 'zura-composer') => '0.1em',
                esc_html__('Letter Spacing: 200', 'zura-composer') => '0.2em',
                esc_html__('Letter Spacing: 300', 'zura-composer') => '0.3em',
                esc_html__('Letter Spacing: 400', 'zura-composer') => '0.4em',
                esc_html__('Letter Spacing: 500', 'zura-composer') => '0.5em'
            ),
            'dependency' => array(
                'element' => 'custom_style',
                'value' => 'true'
            ),
        ),
        array(
            "type" => "colorpicker",
            "heading" => __('Color', 'zura-composer'),
            "param_name" => "color",
            "description" => __('Text color', 'zura-composer'),
            'dependency' => array(
                'element' => 'custom_style',
                'value' => 'true'
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Font Size', 'zura-composer'),
            'param_name' => 'font_size',
            'description' => esc_html__('Ex: 15px', 'zura-composer'),
            'dependency' => array(
                'element' => 'custom_style',
                'value' => 'true'
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Radius', 'zura-composer'),
            'param_name' => 'radius',
            'description' => esc_html__('Ex: 15px 35px 15px 35px.', 'zura-composer'),
            'dependency' => array(
                'element' => 'custom_style',
                'value' => 'true'
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Alignment', 'zura-composer'),
            'param_name' => 'align',
            'description' => esc_html__('Select button alignment.', 'zura-composer'),
            // compatible with btn2, default left to be compatible with btn1
            'value' => array(
                esc_html__('Inline', 'zura-composer') => 'inline',
                esc_html__('Left', 'zura-composer') => 'left',
                esc_html__('Right', 'zura-composer') => 'right',
                esc_html__('Center', 'zura-composer') => 'center'
            ),
            'dependency' => array(
                'element' => 'custom_style',
                'value' => 'true'
            ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Set full width button?', 'zura-composer'),
            'param_name' => 'button_block',
            'dependency' => array(
                'element' => 'align',
                'value_not_equal_to' => 'inline',
            ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Add icon?', 'zura-composer'),
            'param_name' => 'add_icon',
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __('Icon', 'zura-composer'),
            'param_name' => 'icon',
            'value' => '',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),

            'dependency' => array(
                'element' => 'add_icon',
                'value' => 'true',
            ),
            'description' => __('Select icon from library.', 'zura-composer'),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Icon Alignment', 'zura-composer'),
            'description' => esc_html__('Select icon alignment.', 'zura-composer'),
            'param_name' => 'i_align',
            'value' => array(
                esc_html__('Left', 'zura-composer') => 'left',
                // default as well
                esc_html__('Right', 'zura-composer') => 'right',
            ),
            'dependency' => array(
                'element' => 'add_icon',
                'value' => 'true',
            ),
        ),
    ),
    array(
        vc_map_add_css_animation(true),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Extra class name', 'zura-composer'),
            'param_name' => 'el_class',
            'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'zura-composer'),
        ),
    )
);
/**
 * @class WPBakeryShortCode_VC_Btn
 */
vc_map(array(
    'name' => esc_html__('ZU Button', 'zura-composer'),
    'base' => 'zu_btn',
    'category' => array(
        esc_html__('Zura Shortcodes', 'zura-composer'),
    ),
    'description' => esc_html__('Eye catching button', 'zura-composer'),
    'params' => $params,
    'js_view' => 'VcButton3View',
    'custom_markup' => '{{title}}<div class="vc_btn3-container"><button class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-{{ params.shape }} vc_btn3-style-{{ params.style }} vc_btn3-color-{{ params.color }}">{{{ params.title }}}</button></div>',
));


class WPBakeryShortCode_zu_btn extends ZuShortcode
{

    protected function content($attrs, $content = null)
    {
        return parent::content($attrs, $content);
    }
}

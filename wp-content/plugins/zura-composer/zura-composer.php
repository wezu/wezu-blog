<?php
/**
 *
 * Plugin Name: Zura Composer
 * Plugin URI: https://zuravn.com
 * Description: This plugin is package compilation some addons, which is developed by CMSTheme Team for Visual Comporser plugin.
 * Version: 1.0.0
 * Author: Zura Composer
 * Author URI: https://zuravn.com
 * Copyright 2015 zuravn.com. All rights reserved.
 */

define('ZU_DIR', plugin_dir_path(__FILE__));
define('ZU_URL', plugin_dir_url(__FILE__));
define('ZU_LIBRARIES', ZU_DIR . "libraries" . DIRECTORY_SEPARATOR);
define('ZU_LANGUAGES', ZU_DIR . "languages" . DIRECTORY_SEPARATOR);
define('ZU_TEMPLATES', ZU_DIR . "templates" . DIRECTORY_SEPARATOR);
define('ZU_INCLUDES', ZU_DIR . "includes" . DIRECTORY_SEPARATOR);

define('ZU_CSS', ZU_URL . "assets/css/");
define('ZU_JS', ZU_URL . "assets/js/");
define('ZU_IMAGES', ZU_URL . "assets/images/");
/**
 * Require functions on plugin
 */
require_once ZU_INCLUDES . "functions.php";

/**
 * ZuraComposer Class
 *
 */
class ZuraComposer
{
    public function __construct()
    {
        /**
         * Init function, which is run on site init and plugin loaded
         */
        add_action('plugins_loaded', array($this, 'actionInit'));

        /**
         * Enqueue Scripts on plugin
         */
        add_action('wp_enqueue_scripts', array($this, 'registerStyles'));
        add_action('wp_enqueue_scripts', array($this, 'registerScripts'));
        /**
         * Enqueue Scripts into Admin
         */
        add_action('admin_enqueue_scripts', array($this, 'registerStyles'));
        /**
         * Visual Composer action
         */
        add_action('vc_before_init', array($this, 'shortcodeRegister'));
        add_action('vc_after_init', array($this, 'shortcodeAddParams'));
        /**
         * widget text apply shortcode
         */
        add_filter('widget_text', 'do_shortcode');
    }

    function actionInit()
    {
        // Localization
        load_plugin_textdomain('zura-composer', false, ZU_LANGUAGES);
    }

    function shortcodeRegister()
    {
        //Load required libararies
        require_once ZU_INCLUDES . 'zu_shortcodes.php';
    }

    /**
     * Add Shortcode Params
     *
     * @return void
     */
    function shortcodeAddParams()
    {
        $extra_params_folder = get_template_directory() . '/vc_params';
        $files = zuFileScanDirectory($extra_params_folder, '/^zu_.*\.php/');
        if (!empty($files)) {
            foreach ($files as $file) {
                if (WPBMap::exists($file->name)) {
                    include $file->uri;
                    if (isset($params) && is_array($params)) {
                        foreach ($params as $param) {
                            if (is_array($param)) {
                                $param['group'] = __('Template', 'zura-composer');
                                $param['edit_field_class'] = isset($param['edit_field_class'])
                                    ? $param['edit_field_class'] . ' zu_custom_param vc_col-sm-12 vc_column'
                                    : 'zu_custom_param vc_col-sm-12 vc_column';
                                $param['class'] = 'zu-extra-param';
                                if (isset($param['template']) && !empty($param['template'])) {
                                    if (!is_array($param['template'])) {
                                        $param['template'] = array($param['template']);
                                    }
                                    $param['dependency'] = array("element" => "zu_template", "value" => $param['template']);

                                }
                                vc_add_param($file->name, $param);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Function register stylesheet on plugin
     */
    function registerStyles()
    {
        wp_enqueue_style('zu-plugin-stylesheet', ZU_CSS . 'zu-style.css');
    }

    /**
     * Function register script on plugin
     */
    function registerScripts()
    {
        wp_register_script('modernizr', ZU_JS . 'modernizr.min.js', array('jquery'));
        wp_register_script('waypoints', ZU_JS . 'waypoints.min.js', array('jquery'));
        wp_register_script('imagesloaded', ZU_JS . 'jquery.imagesloaded.js', array('jquery'));
        wp_register_script('jquery-shuffle', ZU_JS . 'jquery.shuffle.js', array('jquery', 'modernizr', 'imagesloaded'));
        wp_register_script('zu-jquery-shuffle', ZU_JS . 'jquery.shuffle.zu.js', array('jquery-shuffle'));
    }

}
/**
 * Use ZuraComposer class
 */
new ZuraComposer();

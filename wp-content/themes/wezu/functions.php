<?php
/**
 * @package zura
 */

function my_admin_scripts() {
    wp_enqueue_script( 'admin-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js', true );
}
add_action( 'wp_enqueue_scripts', 'my_admin_scripts' );

if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) :
    require_once dirname(__FILE__) . '/vendor/autoload.php';
endif;

if (class_exists('Zura\\Init')) :
    Zura\Init::registerServices();
    Zura\Init::registerHelpers();
endif;

/* WooCommerce support */
if(class_exists('Woocommerce')){
    require get_template_directory() . '/woocommerce/wc-template-functions.php';
    require get_template_directory() . '/woocommerce/wc-template-hooks.php';
}
require_once get_template_directory() . '/inc/Setup/resize.php';

require_once get_template_directory() . '/post-types/products.php';
require_once get_template_directory() . '/post-types/services.php';
require_once get_template_directory() . '/post-types/projects.php';

// Truncate
function truncate( $text, $chars = 400, $more = false ) {
    $text = strip_tags( $text );
    $see = "<p><a href=". get_the_permalink() ." class='click-popup'>". __('XEM THÊM', 'zura'). "<span class='icon-arrow-bottom'></span></a></p>";
    if ( strlen( $text ) > $chars ) {
        $text = $text." ";
        $text = substr($text,0,$chars);
        $text = substr($text,0,strrpos($text,' '));
        $text = $text."...";
        if ( $more != false) {
          $text = $text.$see;
        }
    }
    return $text;
}

function banner_shortcode() {
    $listBanner = get_field("list_banner_home");
    $html = '';
    if($listBanner):
    $html = '<div class="banner-home">
        <div class="list-slider-banner">';
        foreach($listBanner as $item):
            $true = $item['video'];
            $youtube = $item['youtube'];
            $img = $item['image']['url'];
            $title = $item['title'];
            $content = $item['des'];
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtube, $match);
            if($true === "") {
                $html .= '<div class="item-slider bg-cover" style="background-image: url('. get_image_featured_uri($img, 1920, 550) .')">
                    <img src='. aq_resize($img, 350 , 350, false) .' />
                    <div class="box-content-slider">
                        <div class="container">
                            <h3 class="title title-banner" data-animation="fadeIn fadeInLeft" data-delay="0.6s" data-wow-iteration="infinite">'. $title .'</h3>
                            <p class="des des-banner" data-animation="fadeIn fadeInRightBig" data-delay="0.3s">'. $content .'</p>
                        </div>
                    </div>
                </div>';
            } else {
                $html .= '<div class="item-slider video-background">
                    <div class="video-foreground">
                        <iframe src="https://www.youtube.com/embed/'.$match[1].'?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&mute=1&playlist='.$match[1].'" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="box-content-slider">
                        <div class="container">
                            <h3 class="title title-banner" data-animation="fadeIn fadeInLeft" data-delay="0.6s" data-wow-iteration="infinite">'.$title.'</h3>
                            <p class="des des-banner" data-animation="fadeIn fadeInRightBig" data-delay="0.3s">'.$content.'</p>
                        </div>
                    </div>
                </div>';
            }
        endforeach;
        $html .= '</div></div>';
    endif;
    return $html;
}
add_shortcode('banner', 'banner_shortcode');
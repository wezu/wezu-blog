<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zura
 */
?>
<footer id="footer" class="footer-wrap">
    <?php zu_footer(); ?>
</footer><!-- #footer -->
</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>

<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zura
 */

get_header(); ?>
    <div id="main-content" class="zu-main-content">
        <div class="<?php zu_main_css(); ?>">
            <div class="row">
                <div class="col-md-8">
                    <div id="primary" class="content-area">
                        <?php
                        while (have_posts()) :
                            the_post();

                            get_template_part('views/single/content', get_post_format());

                            the_post_navigation();

                            if (comments_open()) :
                                comments_template();
                            endif;

                        endwhile;
                        ?>
                    </div><!-- #primary -->
                </div><!-- .col- -->
                <div class="col-md-4">
                    <?php get_sidebar(); ?>
                </div><!-- .col- -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- #main-content -->
<?php
get_footer();

<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package zura
 */

get_header(); ?>
    <div id="main-content" class="zu-main-content">
        <div class="<?php zu_main_css(); ?>">
            <div class="row">
                <div class="col-md-8">
                    <div id="primary" class="content-area">
                        <div class="content-404">
                            <h1><?php echo esc_html__('Woops, looks like', 'zura') . '<br class="hidden-xs">' . esc_html__('this page doesn\'t exist', 'zura'); ?></h1>
                            <p><?php esc_html__('You could either go back or go to homepage', 'zura'); ?></p>
                            <p>
                                <a class="btn btn-primary" href="<?php echo esc_url(home_url('/')) ?>">
                                    <?php esc_html_e('Go To Homepage', 'zura'); ?>
                                </a>
                            </p>
                        </div>
                    </div><!-- #primary -->
                </div><!-- .col- -->
                <div class="col-md-4">
                    <?php get_sidebar(); ?>
                </div><!-- .col- -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- #main-content -->
<?php
get_footer();

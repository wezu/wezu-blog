<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zura
 */

get_header(); ?>
    <div id="main-content" class="zu-main-content">
        <div class="<?php zu_main_css(); ?>">
            <div class="row">
                <div class="col-md-8">
                    <div id="primary" class="content-area">
                        <?php
                        if (have_posts()) :
                            while (have_posts()) :

                                the_post();

                                get_template_part('views/content/content', get_post_format());

                            endwhile;

                            the_posts_navigation();

                        else :
                            get_template_part('views/content/content', 'none');
                        endif;
                        ?>
                    </div><!-- #primary -->
                </div><!-- .col- -->
                <div id="sidebar" class="col-md-4">
                    <?php get_sidebar(); ?>
                </div><!-- .col- -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- #main-content -->
<?php
get_footer();

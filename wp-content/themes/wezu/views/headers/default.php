<header id="header" class="site-header default">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-2 col-md-3 col-lg-2 col-logo">
                <?php zu_header_logo(); ?>
            </div><!-- .col -->
            <div class="col-xs-12 col-sm-2 col-md-1 col-lg-2 col-phone">
                <p class="phone-menu"><a href="tel: 01231231231"><i class="icon-telephone"></i><span>01231231231</span></a></p>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="zu-custom-box">
                    <?php zu_cart_mini(); ?>
                    <?php zu_wpml_languages_list(); ?>
                    <?php zu_search_form_mini(); ?>
                </div>
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button"
                            data-toggle="collapse" data-target="#main-navigation"
                            aria-controls="main-navigation" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="main-navigation">
                        <?php
                        if (has_nav_menu('primary')) :
                            wp_nav_menu(array(
                                'theme_location' => 'primary',
                                'menu_id' => 'primary-menu',
                                'menu_class' => 'navbar-nav zu-navigation',
                                'walker' => new Zura\Core\WalkerNav(),
                            ));
                        endif;
                        ?>
                    </div>
                </nav>
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container-fluid -->
</header><!-- #header-->

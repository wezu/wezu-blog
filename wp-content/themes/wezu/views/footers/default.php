<div id="footer-top">
    <div class="container">
        <div class="row">
            <?php if (is_active_sidebar('sidebar-footer-1')) : ?>
                <div class="sidebar-footer-1 col-lg-4 col-md-12 col-sm-12">
                    <?php dynamic_sidebar('sidebar-footer-1'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('sidebar-footer-2')) : ?>
                <div class="sidebar-footer-2 col-lg-4 col-md-6 col-sm-6">
                    <?php dynamic_sidebar('sidebar-footer-2'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('sidebar-footer-3')) : ?>
                <div class="sidebar-footer-3 col-lg-4 col-md-6 col-sm-6">
                    <?php dynamic_sidebar('sidebar-footer-3'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div><!-- Footer Top -->
<div id="footer-bottom">
    <div class="container">
        <div class="row">
            <?php if (is_active_sidebar('sidebar-bottom-1')) : ?>
                <div class="sidebar-bottom-1 col-md-6 col-sm-6">
                    <?php dynamic_sidebar('sidebar-bottom-1'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('sidebar-bottom-2')) : ?>
                <div class="sidebar-bottom-2 col-md-6 col-sm-6 footer-social">
                    <?php dynamic_sidebar('sidebar-bottom-2'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
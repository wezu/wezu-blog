<?php
/**
 * Template part for displaying content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zura
 */

?>

<?php
$byline = sprintf(
    esc_html_x('%s', 'post author', 'zura'),
    '<span class="author vcard"><a class="url fn n" href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a></span>'
);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post-item'); ?>>
    <div class="item-header">
        <div class="item-posted-on">
            <span class="date"><?php echo get_the_time('j'); ?></span>
            <span class="month"><?php echo get_the_time('m-Y'); ?></span>
        </div>
        <div class="item-info">
            <h3 class="item-title">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_title(); ?>
                </a>
            </h3>
            <div class="item-meta">
                <div class="item-posted-by">
                    <span class="item-posted-by-label"><?php _e('Posted by', 'zura'); ?></span>
                    <?php echo $byline; ?>
                </div>
                <?php if(has_category()): ?>
                    <div class="item-category">
                        <span class="item-category-label"><?php _e('Posted in', 'zura'); ?></span>
                        <?php the_terms( get_the_ID(), 'category', '', ' / ' ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="item-body">
        <div class="item-image">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php echo zu_post_thumbnail(800, 450); ?>
            </a>
        </div>
        <div class="item-content">
            <?php the_excerpt(); ?>
        </div>
    </div>
</article><!-- #post-## -->

<?php
global $product;
$args = array(
    'quantity'   => 1,
    'class'      => implode( ' ', array_filter( array(
        'zu-add-to-cart-button btn btn-primary btn-transparent',
        'product_type_' . $product->get_type(),
        $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
        $product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
    ) ) ),
    'attributes' => array(
        'data-product_id'  => $product->get_id(),
        'data-product_sku' => $product->get_sku(),
        'aria-label'       => $product->add_to_cart_description(),
        'rel'              => 'nofollow',
    ),
);
?>
<div class="zu-product-item-style-02 hover-scale" data-product-id="<?php echo get_the_ID() ?>">
    <figure class="zu-product-thumb"><?php echo zu_post_thumbnail(370, 340); ?></figure>
    <div class="zu-product-meta">
        <h3 class="zu-product-title"><a href="<?php echo get_the_permalink() ?>" data-product-id="<?php echo get_the_ID() ?>"><?php echo get_the_title() ?></a></h3>
        <p class="zu-product-description"><?php echo wp_trim_words(get_the_excerpt(), 20); ?></p>
        <?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
            sprintf( '<a href="%s" data-quantity="%s" class="%s" %s>%s</a>',
                esc_url( $product->add_to_cart_url() ),
                esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
                esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
                isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
                esc_html( __('Order Now >', 'zura-composer') )
            ),
            $product, $args );
        ?>
    </div>
</div>
<?php
/**
 * The header for our theme
 *
 * @package zura
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="wrapper" class="zu-wrapper site">
    <?php zu_header(); ?>

    <?php zu_page_element(); ?>

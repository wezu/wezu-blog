/*
 * Zura uses Laravel Mix
 *
 * Check the documentation at
 * https://laravel.com/docs/5.6/mix
 */

let mix = require('laravel-mix');

mix.setPublicPath('./assets');

// Compile assets
mix.js('assets/src/scripts/app.js', 'assets/js')
    .js('assets/src/scripts/admin.js', 'assets/js')
    .sass('assets/src/sass/style.scss', 'assets/css')
    .sass('assets/src/sass/admin.scss', 'assets/css')
    .options({
        processCssUrls: false
    });

//disable alert
mix.disableNotifications();

// Add versioning to assets in production environment
if (mix.inProduction()) {
    mix.version();
}

<?php
// Register Custom Product
function product_post_types() {

  $labels = array(
    'name'                  => _x( 'Product', 'Product General Name', 'zura' ),
    'singular_name'         => _x( 'Product', 'Product Singular Name', 'zura' ),
    'menu_name'             => __( 'Product', 'zura' ),
    'name_admin_bar'        => __( 'Product', 'zura' ),
    'archives'              => __( 'Item Archives', 'zura' ),
    'attributes'            => __( 'Item Attributes', 'zura' ),
    'parent_item_colon'     => __( 'Parent Item:', 'zura' ),
    'all_items'             => __( 'All Items', 'zura' ),
    'add_new_item'          => __( 'Add New Item', 'zura' ),
    'add_new'               => __( 'Add New', 'zura' ),
    'new_item'              => __( 'New Item', 'zura' ),
    'edit_item'             => __( 'Edit Item', 'zura' ),
    'update_item'           => __( 'Update Item', 'zura' ),
    'view_item'             => __( 'View Item', 'zura' ),
    'view_items'            => __( 'View Items', 'zura' ),
    'search_items'          => __( 'Search Item', 'zura' ),
    'not_found'             => __( 'Not found', 'zura' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'zura' ),
    'featured_image'        => __( 'Featured Image', 'zura' ),
    'set_featured_image'    => __( 'Set featured image', 'zura' ),
    'remove_featured_image' => __( 'Remove featured image', 'zura' ),
    'use_featured_image'    => __( 'Use as featured image', 'zura' ),
    'insert_into_item'      => __( 'Insert into item', 'zura' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'zura' ),
    'items_list'            => __( 'Items list', 'zura' ),
    'items_list_navigation' => __( 'Items list navigation', 'zura' ),
    'filter_items_list'     => __( 'Filter items list', 'zura' ),
  );
  $args = array(
    'label'               => __( 'Product', 'zura' ),
    'description'         => __( 'Product Description', 'zura' ),
    'labels'              => $labels,
    'supports'            => ['title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions'],
    'hierarchical'        => false,
    'menu_icon'           => 'dashicons-clipboard',
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'show_in_admin_bar'   => true,
    'show_in_nav_menus'   => true,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'rewrite'           => array( 'slug' => 'san-pham' ),
  );
  register_post_type( 'products', $args );

  $labels = array(
    'name'              => _x( 'Product', 'taxonomy general name', 'zura' ),
    'singular_name'     => _x( 'Product', 'taxonomy singular name', 'zura' ),
    'search_items'      => __( 'Search Product', 'zura' ),
    'all_items'         => __( 'All Product', 'zura' ),
    'parent_item'       => __( 'Parent Product', 'zura' ),
    'parent_item_colon' => __( 'Parent Product:', 'zura' ),
    'edit_item'         => __( 'Edit Product', 'zura' ),
    'update_item'       => __( 'Update Product', 'zura' ),
    'add_new_item'      => __( 'Add New Product', 'zura' ),
    'new_item_name'     => __( 'New Product Name', 'zura' ),
    'menu_name'         => __( 'Category', 'zura' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'danh-muc-san-pham' ),
    );

    register_taxonomy( 'product_cat', array( 'products' ), $args );

}
add_action( 'init', 'product_post_types', 0 );

?>
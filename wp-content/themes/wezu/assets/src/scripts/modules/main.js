(function ($) {
    "use strict";

    var scroll_top = $(window).scrollTop();
    var window_height = $(window).height();
    $('body').on('click', '#back-to-top', function () {
        $("html, body").animate({scrollTop: 0}, 1000);
    });
    $(window).scroll(function () {
        scroll_top = $(window).scrollTop();
        if (scroll_top < window_height) {
            $('#back-to-top').addClass('off').removeClass('on');
        } else {
            $('#back-to-top').removeClass('off').addClass('on');
        }
    });

    //------------ quality --------------//
    /* Plus Qty */
    $(document).on('click', '.qty-plus', function() {
        var parent = $(this).parent();
        $('input.qty', parent).val( parseInt($('input.qty', parent).val()) + 1);
        $('input.qty', parent).trigger('change');
    });
    /* Minus Qty */
    $(document).on('click', '.qty-minus', function() {
        var parent = $(this).parent();
        if( parseInt($('input.qty', parent).val()) > 1) {
            $('input.qty', parent).val( parseInt($('input.qty', parent).val()) - 1);
            $('input.qty', parent).trigger('change');
        }
    });

    $('.list-slider-banner').on('init', function(e, slick) {
        var $firstAnimatingElements = $('div.slick-slide:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);    
    });
    $('.list-slider-banner').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
        var $animatingElements = $('div.slick-slide[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
        doAnimations($animatingElements);    
    });
    $('.list-slider-banner').slick({
        autoplay: true,
        autoplaySpeed: 10000,
        dots: false,
        arrows: false,
        fade: true
    });
    function doAnimations(elements) {
        var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        elements.each(function() {
            var $this = $(this);
            var $animationDelay = $this.data('delay');
            var $animationType = 'animated ' + $this.data('animation');
            $this.css({
                'animation-delay': $animationDelay,
                '-webkit-animation-delay': $animationDelay
            });
            $this.addClass($animationType).one(animationEndEvents, function() {
                $this.removeClass($animationType);
            });
        });
    };
})(jQuery);
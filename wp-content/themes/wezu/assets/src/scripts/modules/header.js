(function ($) {
    "use strict";

    if ($('.header-sticky').length && parseInt($(window).width()) > 768) {
        var $menu = $('.header-sticky'), $menuOff = $menu.offset(), $menuH = $menu.outerHeight();
        $menu.wrap('<div class="header-sticky-wrap"></div>');
        $('.header-sticky-wrap').height($menuH);
        $(window).scroll(function (e) {
            if (($(this).scrollTop() > ($menuOff.top)) && parseInt($(window).width()) > 768) {
                $menu.addClass('header-fixed');
            } else {
                $menu.removeClass('header-fixed');
            }
        });
    }

    $('.menu-arrow').click(function () {
        var $parent = $(this).parent();
        $(this).toggleClass('fa-caret-left fa-caret-down');
        $parent.toggleClass('open');
        return false;
    });

    $('.zu-search-form .search-btn').click(function(){
        $('.zu-search-form form').stop().slideToggle();
    });

    $('.zu-language-wrap .icon').click(function() {
        $(this).toggleClass('open-language');
        $('.zu-language-wrap .language-list').slideToggle();
    });

})(jQuery);
(function ($) {
    "use strict";
    //Shopping Cart Dropdown
    var cartWidget = $('.shopping-cart-wrap');
    if (cartWidget.length) {
        $('.btn-cart', cartWidget).click(function (e) {
            e.preventDefault();
            $('.shopping-cart-dropdown').toggleClass('open');
        });
        cartWidget.on('click', function (e) {
            e.stopPropagation();
        });
        $('body').on('click', function () {
            $('.shopping-cart-dropdown').removeClass('open');
        });
    }
})(jQuery);

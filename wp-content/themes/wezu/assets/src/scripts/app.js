/**
 * Main App Js
 */
import './modules/carousel';
import './modules/cart';
import './modules/header';
import './modules/main';

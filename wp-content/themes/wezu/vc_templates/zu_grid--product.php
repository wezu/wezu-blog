<?php
/* get categories */
$taxonomy = 'category';
$_category = array();
if (!isset($atts['cat']) || $atts['cat'] == '') {
    $terms = get_terms($taxonomy);
    foreach ($terms as $cat) {
        $_category[] = $cat->term_id;
    }
} else {
    $_category = explode(',', $atts['cat']);
}
$atts['categories'] = $_category;
?>
<div class="zu-grid-wraper <?php echo esc_attr($atts['template']); ?>" id="<?php echo esc_attr($atts['html_id']); ?>">

    <?php if (isset($atts['filter']) && $atts['filter'] == 1 && $atts['layout'] == 'masonry'): ?>
        <div class="zu-grid-filter">
            <ul class="zu-filter-category list-unstyled list-inline">
                <li><a class="active" href="#" data-group="all"><?php esc_html_e("All", 'zura-composer'); ?></a></li>
                <?php
                $posts = $atts['posts'];
                $query = $posts->query;
                $taxs = array();
                if (isset($query['tax_query'])) {
                    $tax_query = $query['tax_query'];
                    foreach ($tax_query as $tax) {
                        if (is_array($tax)) {
                            $taxs[] = $tax['terms'];
                        }
                    }
                }
                foreach ($atts['categories'] as $category):
                    if (!empty($taxs)) {
                        if (in_array($category, $taxs)) {
                            $term = get_term($category, $taxonomy);
                            ?>
                            <li><a href="#"
                                   data-group="<?php echo esc_attr('category-' . $term->slug); ?>"><?php echo esc_attr($term->name); ?></a>
                            </li>
                        <?php }
                    } else {
                        $term = get_term($category, $taxonomy);
                        ?>
                        <li><a href="#"
                               data-group="<?php echo esc_attr('category-' . $term->slug); ?>"><?php echo esc_attr($term->name); ?></a>
                        </li>
                        <?php
                    }
                endforeach;
                ?>
            </ul>
        </div>
    <?php endif; ?>

    <div class="row zu-grid <?php echo esc_attr($atts['grid_class']); ?>">
        <?php
        $posts = $atts['posts'];
        $size = (isset($atts['layout']) && $atts['layout'] == 'basic') ? 'thumbnail' : 'medium';
        while ($posts->have_posts()) {
            $posts->the_post();
            $groups = array();
            $groups[] = '"all"';
            foreach (zuGetCategoriesByPostID(get_the_ID(), $taxonomy) as $category) {
                $groups[] = '"category-' . $category->slug . '"';
            }

            $socials = zu_get_field('social_network');
            ?>
            <div class="zu-grid-item <?php echo esc_attr($atts['item_class']); ?>"
                 data-groups='[<?php echo implode(',', $groups); ?>]'>
                <div class="row">
                    <div class="col-sm-12 fix-padding-5">
                        <a class="box-link-hover" href="<?php echo get_the_permalink() ?>">
                            <div class="item-image">
                                <img src="<?php echo get_image_featured(get_the_ID(), 600, 400); ?>" />
                            </div>
                            <div class="box-item-hover">
                                <h3 class="item-title"><?php echo get_the_title(); ?></h3>
                                <div class="item-info"><?php echo truncate(get_the_content(), 100); ?></div>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col-sm-12">
                        <h3 class="item-title"><?php the_title(); ?></h3>
                        <?php if (!empty($socials)): ?>
                            <ul class="item-social">
                                <?php foreach ($socials as $social) : ?>
                                    <li>
                                        <a class="<?php echo $social['type']; ?>"
                                           href="<?php echo $social['url']; ?>" target="_blank">
                                            <?php echo zu_get_social_icon($social['type']) ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <div class="item-info"><?php the_content(); ?></div>
                    </div> -->
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <?php wp_reset_postdata(); ?>
</div>

<div class="zu-fancyboxes-wraper <?php echo esc_attr($attrs['template']); ?>"
     id="<?php echo esc_attr($attrs['html_id']); ?>">
    <?php if ($attrs['title'] != ''): ?>
        <div class="zu-fancyboxes-head">
            <div class="zu-fancyboxes-title">
                <?php echo apply_filters('the_title', $attrs['title']); ?>
            </div>
            <div class="zu-fancyboxes-description">
                <?php echo apply_filters('the_content', $attrs['description']); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="row zu-fancyboxes-body">
        <?php
        $columns = ((int)$attrs['zu_cols']) ? (int)$attrs['zu_cols'] : 1;

        for ($i = 1; $i <= $columns; $i++) { ?>
            <?php if ($i != 5): ?>
                <?php
                $icon = isset($attrs['icon' . $i]) ? $attrs['icon' . $i] : '';
                $content = isset($attrs['description' . $i]) ? $attrs['description' . $i] : '';
                $image = isset($attrs['image' . $i]) ? $attrs['image' . $i] : '';
                $title = isset($attrs['title' . $i]) ? $attrs['title' . $i] : '';
                $button_link = isset($attrs['button_link' . $i]) ? $attrs['button_link' . $i] : '';
                $image_url = '';
                if (!empty($image)) {
                    $attachment_image = wp_get_attachment_image_src($image, 'full');
                    $image_url = $attachment_image[0];
                }
                ?>
                <div class="<?php echo esc_attr($attrs['item_class']); ?>">
                    <?php if ($image_url): ?>
                        <div class="fancy-box-image">
                            <img src="<?php echo esc_attr($image_url); ?>"/>
                        </div>
                    <?php else: ?>
                        <div class="fancy-box-icon">
                            <i class="<?php echo esc_attr($icon); ?>"></i>
                        </div>
                    <?php endif; ?>
                    <?php if ($title): ?>
                        <h3><?php echo apply_filters('the_title', $title); ?></h3>
                    <?php endif; ?>
                    <div class="fancy-box-content">
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>
                    <?php if ($attrs['button_text'] != ''): ?>
                        <div class="zu-fancyboxes-foot">
                            <?php
                            $class_btn = ($attrs['button_type'] == 'button') ? 'btn btn-large' : '';
                            ?>
                            <a href="<?php echo esc_url($button_link); ?>"
                               class="<?php echo esc_attr($class_btn); ?>"><?php echo esc_attr($attrs['button_text']); ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php }
        ?>
    </div>
</div>
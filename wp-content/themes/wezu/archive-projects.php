<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zura
 */
get_header(); ?>
  <div id="main-content-archive" class="zu-main-content">
    <div class="<?php zu_main_css(); ?>">
      <div class="row">
        <div class="col-md-12">
          <div id="primary" class="content-area">
            <div class="box-list-archive">
              <?php if (have_posts()) : ?>
                <?php
                /* Start the Loop */
                while (have_posts()) :
                  the_post();
                  ?>
                  <div class="item-post">
                    <a href="<?php echo get_the_permalink(); ?>">
                      <div class="bg-cover img-news" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>)">
                        <img src="<?php echo get_image_featured(get_the_ID(), 350, 320); ?>">
                      </div>
                    </a>
                    <div class="item-content-archive">
                      <h3 class="title title-post">
                        <a href="<?php echo get_the_permalink(); ?>">
                          <?php echo get_the_title(); ?>
                        </a>
                      </h3>
                      <div class="item-info"><?php echo truncate(get_the_content(), 150, true); ?></div>
                    </div>
                  </div>
                  <?php
                  // get_template_part('views/content/content', get_post_format());
                endwhile;
                the_posts_navigation();
              else :
                get_template_part('views/content/content', 'none');
              endif;
              ?>
            </div>
          </div><!-- #primary -->
        </div><!-- .col- -->
      </div><!-- .row -->
    </div><!-- .container -->
  </div><!-- #main-content -->
<?php
get_footer();

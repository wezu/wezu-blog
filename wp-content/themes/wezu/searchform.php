<?php
/**
 * Template for displaying search forms
 *
 * @package Zura
 * @subpackage ImZon.Net
 * @since ZuraVN.Com 1.0
 */
?>

<div class="zu-search-area">
    <form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
        <input type="text" id="s" name="s" value=""
               placeholder="<?php _e('Search', 'zura'); ?>"
               class="form-control"
               required="required">
        <button class="submit" type="submit"><span class="fa fa-search"></span></button>
    </form>
</div>

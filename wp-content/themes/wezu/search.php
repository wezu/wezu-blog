<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package zura
 */

get_header(); ?>
    <div id="main-content" class="zu-main-content">
        <div class="<?php zu_main_css(); ?>">
            <div class="row">
                <div class="col-sm-8">
                    <div id="primary" class="content-area">
                        <?php
                        if (have_posts()) :
                            while (have_posts()) :

                                the_post();

                                get_template_part('views/content/content', 'search');

                            endwhile;

                            the_posts_navigation();

                        else :
                            get_template_part('views/content/content', 'none');
                        endif;
                        ?>
                    </div><!-- #primary -->
                </div><!-- .col- -->
                <div class="col-sm-4">
                    <?php get_sidebar(); ?>
                </div><!-- .col- -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- #main-content -->
<?php
get_footer();

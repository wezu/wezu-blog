<?php

namespace Zura\Setup;

use Zura\BaseInterface;

/**
 * Sidebar.
 */
class Sidebar implements BaseInterface
{
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register()
    {
        add_action('widgets_init', array($this, 'registerSidebar'));
    }

    /*
        Define the sidebar
    */
    public function registerSidebar()
    {
        register_sidebar(array(
            'name' => esc_html__('Main Sidebar', 'zura'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Default sidebar to add all your widgets.', 'zura'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ));

        register_sidebar(array(
            'name' => esc_html__('Footer Top #1', 'zura'),
            'id' => 'sidebar-footer-1',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'name' => esc_html__('Footer Top #2', 'zura'),
            'id' => 'sidebar-footer-2',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'name' => esc_html__('Footer Top #3', 'zura'),
            'id' => 'sidebar-footer-3',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'name' => esc_html__('Footer Top #4', 'zura'),
            'id' => 'sidebar-footer-4',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'name' => esc_html__('Footer Top #5', 'zura'),
            'id' => 'sidebar-footer-5',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'name' => esc_html__('Footer Bottom #1', 'zura'),
            'id' => 'sidebar-bottom-1',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        ));
        register_sidebar(array(
            'name' => esc_html__('Footer Bottom #2', 'zura'),
            'id' => 'sidebar-bottom-2',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h5 class="widget-title">',
            'after_title' => '</h5>',
        ));
    }
}

<?php

namespace Zura\Setup;

use Zura\BaseInterface;

/**
 * Menus
 */
class Menus implements BaseInterface
{
    /**
     * register default hooks and actions for WordPress
     */
    public function register()
    {
        add_action('after_setup_theme', array($this, 'menus'));
    }

    /**
     * Register all your menus here
     */
    public function menus()
    {
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'zura'),
        ));
    }
}

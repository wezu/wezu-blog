<?php

namespace Zura\Setup;

use Zura\BaseInterface;

class Setup implements BaseInterface
{
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register()
    {
        add_action('after_setup_theme', array($this, 'setup'));
        add_action('after_setup_theme', array($this, 'contentWidth'), 0);
    }

    public function setup()
    {
        /*
         * Register multiple languages
         */
        load_theme_textdomain('zura', get_template_directory() . '/languages');

        /*
         * Default Theme Support options better have
         */

        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('customize-selective-refresh-widgets');

        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        add_theme_support('custom-background', apply_filters('zu_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        /**
         * Activate Post formats if you need
         */
        add_theme_support('post-formats', array(
            'aside',
            'gallery',
            'link',
            'image',
            'quote',
            'status',
            'video',
            'audio',
            'chat',
        ));

        /**
         * Support WooCommerce
         */
        add_theme_support('woocommerce', array(
            'thumbnail_image_width' => 270,
            'single_image_width' => 585,
            'product_grid' => array(
                'default_rows' => 3,
                'min_rows' => 2,
                'max_rows' => 8,
                'default_columns' => 2,
                'min_columns' => 2,
                'max_columns' => 2,
            ),
        ));
        add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );
    }

    /**
     * Define a max content width to allow WordPress to properly resize your images
     */
    public function contentWidth()
    {
        $GLOBALS['content_width'] = apply_filters('content_width', 1440);
    }
}

<?php

namespace Zura\Setup;

use Zura\BaseInterface;

/**
 * Enqueue.
 */
class Enqueue implements BaseInterface
{
    /**
     * register default hooks and actions for WordPress
     */
    public function register()
    {
        add_action('wp_enqueue_scripts', array($this, 'enqueueScripts'));
        add_action('admin_enqueue_scripts', array($this, 'adminEnqueueScripts'));
    }

    /**
     * @throws \Exception
     */
    public function enqueueScripts()
    {
        $this->enqueueCSS();
        $this->enqueueJs();

    }

    public function adminEnqueueScripts()
    {
        wp_enqueue_style('main', mix('css/admin.css'), array(), '1.0.0', 'all');
    }

    private function enqueueJs()
    {
        wp_enqueue_script('bootstrap', get_template_directory_uri() .'/assets/js/bootstrap.js', array('jquery'), '1.0.0', true);
        wp_enqueue_script('main', mix('js/app.js'), array(), '1.0.0', true);

        // Extra
        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }
    }

    private function enqueueCSS()
    {
        wp_enqueue_style('bootstrap', get_template_directory_uri() .'/assets/css/bootstrap.css', array(), '1.0.0', 'all');
        wp_enqueue_style('main', mix('css/style.css'), array(), '1.0.0', 'all');
        //Font Icons
        $fontAwesome = get_template_directory_uri() . '/assets/font-icons/font-awesome/css/font-awesome.min.css';
        wp_enqueue_style('font-awesome', $fontAwesome, array(), false);
    }
}

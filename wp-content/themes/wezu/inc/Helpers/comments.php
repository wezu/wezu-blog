<?php
/**
 * Comment Helpers
 */

if (!function_exists('zu_comment_nav')) {

    /**
     * Comment Navigation
     * @param string $class
     */
    function zu_comment_nav($class = null)
    {
        // Are there comments to navigate through?
        if (get_comment_pages_count() > 1 && get_option('page_comments')) :
            ?>
            <nav class="navigation comment-navigation <?php echo $class; ?>" role="navigation">
                <div class="nav-links">
                    <?php
                    if ($prev_link = get_previous_comments_link(esc_html__('Older Comments', 'zura'))) :
                        printf('<div class="nav-previous">%s</div>', $prev_link);
                    endif;

                    if ($next_link = get_next_comments_link(esc_html__('Newer Comments', 'zura'))) :
                        printf('<div class="nav-next">%s</div>', $next_link);
                    endif;
                    ?>
                </div><!-- .nav-links -->
            </nav><!-- .comment-navigation -->
        <?php
        endif;
    }
}


if (!function_exists('zu_comment')) {
    /**
     * Add Custom Comment
     * @param $comment
     * @param $args
     * @param $depth
     */
    function zu_comment($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);

        if ('div' == $args['style']) {
            $tag = 'div';
            $add_below = 'comment';
        } else {
            $tag = 'li';
            $add_below = 'div-comment';
        }

        ?>
        <<?php echo esc_attr($tag) ?><?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
        <?php if ('div' != $args['style']) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body clearfix">
    <?php endif; ?>

        <div class="comment-author-image vcard">
            <?php echo get_avatar($comment, 109); ?>
        </div>

        <div class="comment-main">
            <div class="comment-content">
                <?php comment_text(); ?>
            </div>

            <div class="comment-meta">
                <?php printf(wp_kses(__('<span class="comment-author">%s</span>', 'zura'), array('span' => array('class' => array()))), get_comment_author_link()); ?>
                <span class="comment-date"> -
                    <?php printf(_x('%s ago', '%s = human-readable time difference', 'zura'), human_time_diff(get_comment_time('U'), current_time('timestamp'))); ?>
			</span>

                <?php if ($comment->comment_approved == '0') : ?>
                    <em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'zura'); ?></em>
                <?php endif; ?>

                <div class="reply">
                    <?php comment_reply_link(
                        array_merge($args, array(
                                'add_below' => $add_below,
                                'depth' => $depth,
                                'max_depth' => $args['max_depth'])
                        )
                    ); ?>

                </div>
            </div>
        </div>
        <?php if ('div' != $args['style']) : ?>
        </div>
    <?php endif; ?>
        <?php
    }
}
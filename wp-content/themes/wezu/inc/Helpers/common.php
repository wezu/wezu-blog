<?php
/**
 * Helpers methods
 * List all your static functions you wish to use globally on your theme
 *
 * @package zura
 */

if (!function_exists('dd')) {
    /**
     * Var_dump and die method
     *
     * @return void
     */
    function dd()
    {
        echo '<pre>';
        array_map(function ($x) {
            var_dump($x);
        }, func_get_args());
        echo '</pre>';
        die;
    }
}

if (!function_exists('starts_with')) {
    /**
     * Determine if a given string starts with a given substring.
     *
     * @param  string $haystack
     * @param  string|array $needles
     * @return bool
     */
    function starts_with($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle != '' && substr($haystack, 0, strlen($needle)) === (string)$needle) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('mix')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string $path
     * @param  string $manifestDirectory
     *
     * @return string
     * @throws \Exception
     */
    function mix($path, $manifestDirectory = '')
    {
        if (!$manifestDirectory) {
            $manifestDirectory = "assets";
        }
        $manifest = null;
        if (!starts_with($path, '/')) {
            $path = "/{$path}";
        }
        if ($manifestDirectory && !starts_with($manifestDirectory, '/')) {
            $manifestDirectory = "/{$manifestDirectory}";
        }
        $rootDir = get_template_directory();
        if (!$manifest) {
            $manifestPath = $rootDir . $manifestDirectory . '/mix-manifest.json';
            if (!file_exists($manifestPath)) {
                throw new Exception('The Mix manifest does not exist.');
            }
            $manifest = json_decode(file_get_contents($manifestPath), true);
        }
        $path = $manifestDirectory . $manifest[$path];
        return get_template_directory_uri() . $path;
    }
}


/**
 * Removes the demo link and the notice of integrated demo from the redux-framework plugin
 */
if (!function_exists('remove_demo')) {
    function remove_demo()
    {
        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if (class_exists('ReduxFrameworkPlugin')) {
            remove_filter('plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2);

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
        }
    }
}

if (!function_exists('zu_theme_option')) {
    /**
     * Get Theme Option Data
     *
     * @param      $key
     * @param null $default
     *
     * @return null|string
     */
    function zu_theme_option($key, $default = null)
    {
        global $zu_options;

        return isset($zu_options[$key]) ? $zu_options[$key] : $default;
    }
}

if (!function_exists('zu_get_field')) {
    /**
     *  Return a custom field value for a specific field name/key + post_id.
     *
     * @param $selector
     * @param bool $postId
     * @param bool $formatValue
     * @return null|string
     */
    function zu_get_field($selector, $postId = false, $formatValue = true)
    {
        if (!function_exists('get_field')) {
            return null;
        }
        return get_field($selector, $postId, $formatValue);
    }
}


if (!function_exists('zu_the_field')) {
    /**
     *  echo zu_get_field()
     *
     * @param $selector
     * @param bool $postId
     * @param bool $formatValue
     * @return null|string
     */
    function zu_the_field($selector, $postId = false, $formatValue = true)
    {
        $value = zu_get_field($selector, $postId, $formatValue);
        if (is_array($value)) {
            $value = @implode(', ', $value);
        }
        echo $value;
    }
}


if (!function_exists('zu_post_thumbnail')) {
    /**
     * Crop Image Size
     *
     * @param null $width
     * @param null $height
     * @param bool $crop
     * @param bool $single
     * @param bool $upscale
     * @return null|string
     */
    function zu_post_thumbnail($width, $height, $crop = true, $single = true, $upscale = true)
    {
        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
        if (function_exists('zu_image_resize') && isset($image_url[0])) {
            $url = zu_image_resize($image_url[0], $width, $height, $crop, $single, $upscale);
        } else {
            $url = zu_default_image_url();
        }
        return '<img src="' . esc_url($url) . '" alt="' . get_the_title() . '" />';
    }
}

if (!function_exists('zu_default_image_url')) {
    /**
     * Get default post type
     *
     * @return null|string
     */
    function zu_default_image_url()
    {
        $postType = get_post_type(get_the_ID());
        switch ($postType) {
            case 'post':
                $url = get_template_directory_uri() . '/assets/images/default-blog.jpg';
                break;
            default:
                $url = get_template_directory_uri() . '/assets/images/default.jpg';
                break;
        }

        return $url;
    }
}

if (!function_exists('zo_limit_words')) {
    /**
     * Limit Words
     *
     * @param $string
     * @param $word_limit
     * @return mixed|void
     */
    function zu_limit_words($string, $word_limit)
    {
        $words = explode(' ', strip_tags($string), ($word_limit + 1));
        if (count($words) > $word_limit) {
            array_pop($words);
        }
        return apply_filters('the_excerpt', implode(' ', $words));
    }
}


function zu_get_social_icon($type)
{
    switch ($type) {
        case 'facebook':
            return '<i class="fa fa-facebook"></i>';
        case 'google':
            return '<i class="fa fa-google-plus"></i>';
        case 'twitter':
            return '<i class="fa fa-twitter"></i>';
        case 'instagram':
            return '<i class="fa fa-linkedin"></i>';
        case 'pinterest':
            return '<i class="fa fa-pinterest"></i>';
        case 'linkedin':
            return '<i class="fa fa-linkedin"></i>';
        default:
            return '';
    }

}
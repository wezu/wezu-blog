<?php
/**
 * WooCommerce Helpers
 */

if (!function_exists('zu_cart_items')) {
    /**
     * Return a shopping cart
     *
     * @return null
     */
    function zu_cart_items()
    {
        if (!function_exists('WC')) {
            return null;
        }
        ?>
        <div class="zu-cart-wrap">
            <a href="javascript:void(0)" class="btn btn-cart icon">
                <i class="fa fa-shopping-cart cart-icon"></i>
                <span class="cart-total"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
            </a>

            <div class="shopping-cart-dropdown">
                <div class="inner">
                    <ul class="cart-list">
                        <?php if (sizeof(WC()->cart->get_cart()) > 0) : ?>

                            <?php foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) :

                                $product = $cart_item['data'];
                                // Only display if allowed
                                if (!$product->exists() || $cart_item['quantity'] == 0) {
                                    continue;
                                }
                                // Get price
                                $product_price = get_option('woocommerce_tax_display_cart') == 'excl' ? wc_get_price_excluding_tax($product) : '';
                                $product_price = apply_filters('woocommerce_cart_item_price_html', wc_price($product_price), $cart_item, $cart_item_key);
                                $remove_link = '<a href="' . esc_url(WC()->cart->get_remove_url($cart_item_key)) . '" title="Xóa sản phẩm"> &times; </a>';
                                ?>
                                <li class="cart-item clearfix">
                                    <a class="cart-image" href="<?php echo get_permalink($cart_item['product_id']); ?>">
                                        <?php echo $product->get_image(); ?>
                                    </a>
                                    <div class="cart-info">
                                        <div class="title"><a
                                                    href="<?php echo get_permalink($cart_item['product_id']); ?>">
                                                <?php echo apply_filters('woocommerce_widget_cart_product_title', $product->get_title(), $product); ?>
                                            </a></div>
                                        <?php echo $product_price; ?>
                                        <?php echo apply_filters('woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf('QTY: %s %s', $cart_item['quantity'], $remove_link) . '</span>', $cart_item, $cart_item_key); ?>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <li class="cart-item clearfix empty"><?php esc_html_e('No products in the cart.', 'zura'); ?></li>
                        <?php endif; ?>
                    </ul>
                    <?php if (sizeof(WC()->cart->get_cart()) > 0) : ?>
                        <div class="cart-footer">
                            <div class="cart-total">
                                <?php esc_html_e('Sub Total', 'zura'); ?>:
                                <strong><?php echo do_shortcode(WC()->cart->get_cart_subtotal()); ?></strong>
                            </div>
                            <div class="cart-action">
                                <a href="<?php echo esc_url(wc_get_checkout_url()); ?>"
                                   class="btn btn-primary btn-checkout">
                                    <?php esc_html_e('Process To Checkout', 'zura'); ?>
                                </a>
                                <a href="<?php echo esc_url(wc_get_cart_url()); ?>"
                                   class="btn btn-info">
                                    <?php esc_html_e('View Shopping Cart', 'zura'); ?>
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
    }
}


if (!function_exists('zu_cart_mini')) {
    /**
     * Return a shopping cart mini
     *
     * @return null
     */
    function zu_cart_mini()
    {
        if (!function_exists('WC')) {
            return null;
        }
        ?>
        <div class="zu-cart-mini-wrap">
            <a href="<?php echo esc_url(wc_get_cart_url()); ?>" class="cart-item">
                <span class="cart-title"><?php _e('Cart', 'zura') ?></span>
                <span class="cart-total"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
            </a>
        </div>
        <?php
    }
}

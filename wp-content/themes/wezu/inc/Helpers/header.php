<?php

use Zura\Core\Header;

if (!function_exists('zu_page_title_background')) {
    /**
     * Get custom page title background css
     *
     * @return string
     */
    function zu_page_title_background_css()
    {
        $background = zu_get_field('background');
        if (is_home()) {
            $background = zu_get_field('background', get_option('page_for_posts'));
        }
        if (is_tax()) {
            $termObj = get_queried_object();
            $background = get_field('banner', $termObj);
        }
        if (is_single()) {
            $background = get_the_post_thumbnail_url();
        }
        if (is_page() && !is_home()) {
            $background = get_the_post_thumbnail_url(get_the_ID());
        }
        if (!empty($background)) {
            return 'background-image: url(' . $background . ')';
        }
    }
}


if (!function_exists('zu_page_element')) {
    /**
     * Display page elements: Page title, Page Breadcrumbs
     */
    function zu_page_element()
    {
        $hiddenPagetitle = false;
        if (is_front_page()) {
            $hiddenPagetitle = zu_get_field('hidden', get_option('page_on_front'));
        }
        if ($hiddenPagetitle) {
            return;
        }
        $subtitle = zu_get_field('subtitle');
        $title = zu_get_field('title');
        ?>
        <section id="page-title" style="<?php echo zu_page_title_background_css(); ?>">
            <div class="container">
                <div class="page-title-inner">
                    <h1 class="title">
                        <?php echo !empty($title) ? $title : Header::getPageTitle(); ?>
                    </h1>
                    <?php if (!empty($subtitle)) : ?>
                        <div class="sub-title"><?php echo $subtitle; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <?php
    }
}


if (!function_exists('zu_header')) {
    /**
     * Display header template
     */
    function zu_header()
    {
        get_template_part('views/headers/default');
    }
}


if (!function_exists('zu_header_logo')) {
    /**
     * Get Header Logo
     */
    function zu_header_logo()
    {
        $logoUrl = get_template_directory_uri() . '/logo.png';
        $mainLogo = zu_theme_option('main_logo');
        if (isset($mainLogo['url'])) {
            $logoUrl = $mainLogo['url'];
        }

        if (is_front_page()) : ?>
            <h1 id="logo" class="navbar-brand">
                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home" title="<?php bloginfo('name'); ?>">
                    <span class="site-title"><?php bloginfo('name'); ?></span>
                    <img src="<?php echo esc_url($logoUrl); ?>" alt="<?php bloginfo('name'); ?>">
                </a>
            </h1>
        <?php else : ?>
            <p id="logo" class="navbar-brand">
                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home" title="<?php bloginfo('name'); ?>">
                    <span class="site-title"><?php bloginfo('name'); ?></span>
                    <img src="<?php echo esc_url($logoUrl); ?>" alt="<?php bloginfo('name'); ?>">
                </a>
            </p>
        <?php endif;
    }
}


if (!function_exists('zu_main_css')) {
    /**
     * Get custome page full with
     *
     * @param null $class
     * @return string
     */
    function zu_main_css($class = null)
    {
        $fullWidth = zu_get_field('full_width');
        if (is_page() && $fullWidth) {
            $mainCss = 'container-fluid';
        } else {
            $mainCss = 'container';
        }

        if ($class) {
            $mainCss .= ' ' . esc_attr($class);
        }

        echo apply_filters('zu_main_css', $mainCss);
    }
}

if (!function_exists('zu_search_form_mini')) {
    /**
     * Get search form mini
     *
     * @param null $class
     * @return string
     */
    function zu_search_form_mini()
    {
        ?>
        <div class="zu-search-form">
            <span class="search-btn"><i class="fa fa-search"></i></span>
            <form role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
                <input type="text" id="s" name="s" value=""
                       placeholder="<?php _e('Search ...', 'zura'); ?>"
                       class="form-control" required="required">
            </form>
        </div>
        <?php
    }
}


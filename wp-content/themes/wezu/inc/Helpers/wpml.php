<?php
/**
 * WPML Plugin Helpers
 */

function zu_wpml_languages_list()
{
    if (!class_exists('SitePress')) {
        return;
    }
    $languages = apply_filters('wpml_active_languages', NULL, 'skip_missing=0&orderby=code');
    if (!empty($languages)) {
        echo '<div class="zu-language-wrap">';
        echo '<span class="icon"><i class="fa fa-globe"></i></span>';
        echo '<ul class="language-list">';
        foreach ($languages as $l) {
            echo '<li>';
            if ($l['country_flag_url']) {
                if (!$l['active']) echo '<a href="' . $l['url'] . '">';
                echo '<img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" />';
                if (!$l['active']) echo '</a>';
            }
            if (!$l['active']) echo '<a href="' . $l['url'] . '">';
            echo apply_filters('wpml_display_language_names', NULL, $l['native_name']);
            if (!$l['active']) echo '</a>';
            echo '</li>';
        }
        echo '</ul>';
        echo '</div>';
    }
}

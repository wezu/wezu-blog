<?php

namespace Zura\Plugins;

use Zura\BaseInterface;

class Cpt implements BaseInterface
{
    /**
     * register default hooks and actions for WordPress
     * @return void
     */
    public function register()
    {
        add_action('init', [$this, 'registerTaxonomies']);
    }

    public function registerTaxonomies() {

        /**
         * Taxonomy: Team Taxonomies.
         */

        $labels = array(
            "name" => __( "Team Taxonomies", "zura" ),
            "singular_name" => __( "Team Taxonomy", "zura" ),
        );

        $args = array(
            "label" => __( "Team Taxonomies", "zura" ),
            "labels" => $labels,
            "public" => true,
            "hierarchical" => true,
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => array( 'slug' => 'team_taxonomy', 'with_front' => true, ),
            "show_admin_column" => false,
            "show_in_rest" => false,
            "rest_base" => "team_taxonomy",
            "show_in_quick_edit" => false,
        );
        register_taxonomy( "team_taxonomy", array( "team" ), $args );
    }
}

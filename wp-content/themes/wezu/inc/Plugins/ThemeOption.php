<?php
/**
 * Theme Options
 *
 * @package zura
 */

namespace Zura\Plugins;

use ReduxFrameworkPlugin;
use Zura\BaseInterface;
use Redux;

class ThemeOption implements BaseInterface
{

    public $optName = 'zu_options';

    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register()
    {
        if (!class_exists('Redux')) {
            return true;
        }
        add_action('redux/loaded', [$this, 'removeDemo']);

        $this->setup();
        $this->getSections();
    }

    public function removeDemo()
    {
        if (class_exists('ReduxFrameworkPlugin')) {
            remove_filter('plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2);

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
        }
    }

    public function setup()
    {
        $theme = wp_get_theme();
        $args = array(
            // TYPICAL -> Change these values as you need/desire
            'opt_name' => $this->optName,
            // This is where your data is stored in the database and also becomes your global variable name.
            'display_name' => $theme->get('Name'),
            // Name that appears at the top of your panel
            'display_version' => $theme->get('Version'),
            // Version that appears at the top of your panel
            'menu_type' => 'menu',
            //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
            'allow_sub_menu' => true,
            // Show the sections below the admin menu item or not
            'menu_title' => __('Theme Options', 'zura'),
            'page_title' => __('Theme Options', 'zura'),
            // You will need to generate a Google API key to use this feature.
            // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
            'google_api_key' => '',
            // Set it you want google fonts to update weekly. A google_api_key value is required.
            'google_update_weekly' => false,
            // Must be defined to add google fonts to the typography module
            'async_typography' => false,
            // Use a asynchronous font on the front end or font string
            //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
            'admin_bar' => true,
            // Show the panel pages on the admin bar
            'admin_bar_icon' => 'dashicons-portfolio',
            // Choose an icon for the admin bar menu
            'admin_bar_priority' => 50,
            // Choose an priority for the admin bar menu
            'global_variable' => '',
            // Set a different name for your global variable other than the opt_name
            'dev_mode' => false,
            // Show the time the page took to load, etc
            'update_notice' => true,
            // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
            'customizer' => true,
            // Enable basic customizer support
            //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
            //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

            // OPTIONAL -> Give you extra features
            'page_priority' => null,
            // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
            'page_parent' => 'themes.php',
            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
            'page_permissions' => 'manage_options',
            // Permissions needed to access the options panel.
            'menu_icon' => '',
            // Specify a custom URL to an icon
            'last_tab' => '',
            // Force your panel to always open to a specific tab (by id)
            'page_icon' => 'icon-themes',
            // Icon displayed in the admin panel next to your menu_title
            'page_slug' => '',
            // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
            'save_defaults' => true,
            // On load save the defaults to DB before user clicks save or not
            'default_show' => false,
            // If true, shows the default value next to each field that is not the default value.
            'default_mark' => '',
            // What to print by the field's title if the value shown is default. Suggested: *
            'show_import_export' => true,
            // Shows the Import/Export panel when not used as a field.

            // CAREFUL -> These options are for advanced use only
            'transient_time' => 60 * MINUTE_IN_SECONDS,
            'output' => true,
            // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
            'output_tag' => true,
            // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
            // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
            'database' => '',
            // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
            'use_cdn' => true,
            // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

            // HINTS
            'hints' => array(
                'icon' => 'el el-question-sign',
                'icon_position' => 'right',
                'icon_color' => 'lightgray',
                'icon_size' => 'normal',
                'tip_style' => array(
                    'color' => 'red',
                    'shadow' => true,
                    'rounded' => false,
                    'style' => '',
                ),
                'tip_position' => array(
                    'my' => 'top left',
                    'at' => 'bottom right',
                ),
                'tip_effect' => array(
                    'show' => array(
                        'effect' => 'slide',
                        'duration' => '500',
                        'event' => 'mouseover',
                    ),
                    'hide' => array(
                        'effect' => 'slide',
                        'duration' => '500',
                        'event' => 'click mouseleave',
                    ),
                ),
            ),
        );
        Redux::setArgs($this->optName, $args);
    }

    /**
     * Set Section
     * @param $data
     */
    public function addSection($data)
    {
        Redux::setSection($this->optName, $data);
    }

    public function getSections()
    {
        $this->addHeaderSection();
        $this->addTypographySection();
        $this->addPageElementSection();
        $this->addFooterSection();
        $this->addBannerArchive();
    }

    private function addHeaderSection()
    {
        $this->addSection(array(
            'title' => esc_html__('Header', 'zura'),
            'icon' => 'el-icon-picture',
            'fields' => array(
                array(
                    'title' => esc_html__('Select Logo', 'zura'),
                    'subtitle' => esc_html__('Select an image file for your logo.', 'zura'),
                    'id' => 'main_logo',
                    'type' => 'media',
                    'url' => true,
                    'default' => array(
                        'url' => get_template_directory_uri() . '/logo.png'
                    )
                ),
                array(
                    'subtitle' => esc_html__('Display sticky menu when scroll down.', 'zura'),
                    'id' => 'sticky_logo_enable',
                    'type' => 'switch',
                    'title' => esc_html__('Enable Sticky Menu ', 'zura'),
                    'default' => false,
                )
            )
        ));
    }

    private function addTypographySection()
    {
        $this->addSection(array(
            'title' => esc_html__('Typography', 'zura'),
            'icon' => 'el-icon-text-width',
            'fields' => array(
                array(
                    'id' => 'font_body',
                    'type' => 'typography',
                    'title' => esc_html__('Body Font', 'zura'),
                    'google' => true,
                    'font-backup' => true,
                    'all_styles' => true,
                    'output' => array('body'),
                    'units' => 'px',
                    'default' => array(
                        'color' => '#13100e',
                        'font-style' => 'regular',
                        'font-weight' => '400',
                        'font-family' => 'Abel',
                        'google' => true,
                        'font-size' => '16px',
                        'line-height' => '20px',
                        'text-align' => ''
                    ),
                    'subtitle' => esc_html__('Typography option with each property can be called individually.', 'zura'),
                ),
                array(
                    'id' => 'font_h1',
                    'type' => 'typography',
                    'title' => esc_html__('H1', 'zura'),
                    'google' => true,
                    'font-backup' => true,
                    'all_styles' => true,
                    'output' => array('body h1'),
                    'units' => 'px',
                    'default' => array(
                        'color' => '#32302f',
                        'font-weight' => '500',
                        // 'font-family' => 'Oswald',
                        'google' => true,
                        'font-size' => '64px',
                        'line-height' => '1.5',
                        'text-align' => ''
                    )
                ),
                array(
                    'id' => 'font_h2',
                    'type' => 'typography',
                    'title' => esc_html__('H2', 'zura'),
                    'google' => true,
                    'font-backup' => true,
                    'all_styles' => true,
                    'output' => array('body h2'),
                    'units' => 'px',
                    'default' => array(
                        'color' => '#32302f',
                        'font-weight' => '500',
                        'font-family' => 'Oswald',
                        'google' => true,
                        'font-size' => '32px',
                        'line-height' => '38px',
                        'text-align' => ''
                    )
                ),
                array(
                    'id' => 'font_h3',
                    'type' => 'typography',
                    'title' => esc_html__('H3', 'zura'),
                    'google' => true,
                    'font-backup' => true,
                    'all_styles' => true,
                    'output' => array('body h3'),
                    'units' => 'px',
                    'default' => array(
                        'color' => '#32302f',
                        'font-weight' => '500',
                        'font-family' => 'Oswald',
                        'google' => true,
                        'font-size' => '24px',
                        'line-height' => '30px',
                        'text-align' => ''
                    )
                ),
                array(
                    'id' => 'font_h4',
                    'type' => 'typography',
                    'title' => esc_html__('H4', 'zura'),
                    'google' => true,
                    'font-backup' => true,
                    'all_styles' => true,
                    'output' => array('body h4'),
                    'units' => 'px',
                    'default' => array(
                        'color' => '#32302f',
                        'font-weight' => '500',
                        'font-family' => 'Oswald',
                        'google' => true,
                        'font-size' => '20px',
                        'line-height' => '26px',
                        'text-align' => ''
                    )
                ),
                array(
                    'id' => 'font_h5',
                    'type' => 'typography',
                    'title' => esc_html__('H5', 'zura'),
                    'google' => true,
                    'font-backup' => true,
                    'all_styles' => true,
                    'output' => array('body h5'),
                    'units' => 'px',
                    'default' => array(
                        'color' => '#32302f',
                        'font-weight' => '500',
                        'font-family' => 'Oswald',
                        'google' => true,
                        'font-size' => '18px',
                        'line-height' => '24px',
                        'text-align' => ''
                    )
                ),
                array(
                    'id' => 'font_h6',
                    'type' => 'typography',
                    'title' => esc_html__('H6', 'zura'),
                    'google' => true,
                    'font-backup' => true,
                    'all_styles' => true,
                    'output' => array('body h6'),
                    'units' => 'px',
                    'default' => array(
                        'color' => '#32302f',
                        'font-weight' => '500',
                        'font-family' => 'Oswald',
                        'google' => true,
                        'font-size' => '16px',
                        'line-height' => '22px',
                        'text-align' => ''
                    )
                )
            )
        ));
    }

    private function addPageElementSection()
    {
        $this->addSection(array(
            'title' => esc_html__('Page Elements', 'zura'),
            'icon' => 'el-icon-picture',
            'fields' => array(
                array(
                    'id' => 'page_title_height',
                    'type' => 'dimensions',
                    'units' => array('px'),
                    'title' => __('Page title Height', 'zura'),
                    'width' => false,
                    'output' => array('#page-title .container'),
                    'default' => array(
                        'height' => 400
                    ),
                ),
                array(
                    'title' => esc_html__('Background', 'zura'),
                    'id' => 'page_title_background',
                    'type' => 'background',
                    'output' => array('#page-title'),
                    'default' => array(
                        'background-image' => get_template_directory_uri() . '/assets/images/page-title.jpg',
                        'background-color' => '#222',
                        'background-repeat' => 'no-repeat',
                        'background-position' => 'center center',
                        'background-size' => 'cover',
                    ),
                )
            )
        ));
    }

    private function addFooterSection()
    {
        $this->addSection(array(
            'title' => esc_html__('Footer', 'zura'),
            'icon' => 'el-icon-picture',
            'fields' => array(
                array(
                    'title' => esc_html__('Padding', 'zura'),
                    'id' => 'footer_spacing',
                    'type' => 'spacing',
                    'units' => 'px',
                    'left' => false,
                    'right' => false,
                    'mode' => 'padding',
                    'output' => array('#footer'),
                    'default' => array(
                        'padding-top' => '30px',
                        'padding-bottom' => '30px'
                    )
                ),
                array(
                    'title' => esc_html__('Background', 'zura'),
                    'id' => 'footer_background',
                    'type' => 'background',
                    'output' => array('#footer'),
                    'default' => array(
                        'background-image' => get_template_directory_uri() . '/assets/images/bg-footer.jpg',
                        'background-color' => '#0d0806',
                        'background-repeat' => 'no-repeat',
                        'background-position' => 'center center',
                        'background-size' => 'cover',
                    ),
                )
            ),

        ));
    }

    private function addBannerArchive()
    {
        $this->addSection(array(
            'title' => esc_html__('Banner chuyên mục', 'zura'),
            'icon' => 'el-icon-picture',
            'fields' => array(
                array(
                    'title' => esc_html__('Background', 'zura'),
                    'id' => 'bannerArchive',
                    'type' => 'background',
                    'output' => array('#bannerArchive'),
                    'default' => array(
                        'background-image' => get_template_directory_uri() . '/assets/images/bg-footer.jpg',
                        'background-color' => '#0d0806',
                        'background-repeat' => 'no-repeat',
                        'background-position' => 'center center',
                        'background-size' => 'cover',
                    ),
                )
            ),

        ));
    }
}

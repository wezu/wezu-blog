<?php

namespace Zura\Plugins\PluginActivation;

use Zura\BaseInterface;

require_once dirname(__FILE__) . '/class-tgm-plugin-activation.php';

class PluginActivation implements BaseInterface
{
    public function register()
    {
        add_action('tgmpa_register', [$this, 'registerRequiredPlugins']);
    }

    function registerRequiredPlugins()
    {
        /*
         * Array of plugin arrays. Required keys are name and slug.
         * If the source is NOT from the .org repo, then source is also required.
         */
        $root = 'http://wp.zuratheme.com/plugins';

        $plugins = array(
            array(
                'name' => esc_html__('Redux Framework', 'zura'),
                'slug' => 'redux-framework',
                'required' => true,
            ),
            array(
                'name' => esc_html__('Zura Composer', 'zura'),
                'slug' => 'zura-composer',
                'source' => $root . '/zura-composer.zip',
                'required' => true,
            ),
            array(
                'name' => esc_html__('Custom Post Type UI', 'zura'),
                'slug' => 'custom-post-type-ui',
                'required' => true,
            ),
            array(
                'name' => esc_html__('Advanced Custom Fields Pro', 'zura'),
                'slug' => 'advanced-custom-fields-pro',
                'source' => $root . '/acfp.zip',
                'required' => true,
            ),
            array(
                'name' => esc_html__('Visual Composer', 'zura'),
                'slug' => 'js_composer',
                'source' => $root . '/js_composer.zip',
                'required' => true,
            ),
            array(
                'name' => esc_html__('Slider Revolution', 'zura'),
                'slug' => 'revslider',
                'source' => $root . '/revslider.zip',
                'required' => true,
            ),
            array(
                'name' => esc_html__('Contact Form 7', 'zura'),
                'slug' => 'contact-form-7',
                'required' => false,
            ),
            array(
                'name' => esc_html__('WooCommerce', 'zura'),
                'slug' => 'woocommerce',
                'required' => false,
            ),
            array(
                'name' => esc_html__('WPML Multilingual CMS', 'zura'),
                'slug' => 'sitepress-multilingual-cms',
                'source' => $root . '/wpml/sitepress-multilingual-cms.zip',
                'required' => false,
            ),
            array(
                'name' => esc_html__('WPML String Translation', 'zura'),
                'slug' => 'wpml-string-translation',
                'source' => $root . '/wpml/wpml-string-translation.zip',
                'required' => false,
            ),
            array(
                'name' => esc_html__('WPML Translation Management', 'zura'),
                'slug' => 'wpml-translation-management',
                'source' => $root . '/wpml/wpml-translation-management.zip',
                'required' => false,
            ),
            array(
                'name' => esc_html__('WooCommerce Multilingual', 'zura'),
                'slug' => 'woocommerce-multilingual',
                'required' => false,
            ),
        );

        $config = array(
            'id' => 'zura',                 // Unique ID for hashing notices for multiple instances of TGMPA.
            'default_path' => '',                      // Default absolute path to bundled plugins.
            'menu' => 'zura-install-plugins', // Menu slug.
            'parent_slug' => 'themes.php',            // Parent menu slug.
            'capability' => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
            'has_notices' => true,                    // Show admin notices or not.
            'dismissable' => true,                    // If false, a user cannot dismiss the nag message.
            'dismiss_msg' => '',                      // If 'dismissable' is false, this message will be output at top of nag.
            'is_automatic' => false,                   // Automatically activate plugins after installation or not.
            'message' => '',                      // Message to output right before the plugins table.

        );

        tgmpa($plugins, $config);
    }
}

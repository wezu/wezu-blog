<?php

namespace Zura\Core;

/**
 * Tags.
 */
class Header
{
    public static function getPageTitle()
    {
        ob_start();
        if (is_front_page()) {
            esc_html_e('Home', 'zura');
        } elseif (is_home()) {
            esc_html_e('Blog', 'zura');
        } elseif (is_search()) {
            esc_html_e('Search', 'zura');
        } elseif (is_404()) {
            esc_html_e('Page Not Found ', 'zura');
        } elseif (is_tax()) {
            $termObj = get_queried_object();
            esc_html_e($termObj->name, 'zura');
        } elseif (is_archive()) {
            if (is_category()) {
                single_cat_title();
            } elseif (class_exists('Woocommerce')) {
                if (is_shop()) {
                    esc_html_e('Shop', 'zura');
                } elseif (is_woocommerce()) {
                    woocommerce_page_title();
                } else {
                    the_title();
                }
            } elseif (is_tag()) {
                single_tag_title();
            } elseif (is_author()) {
                printf(__('Author: %s', 'zura'), '<span class="vcard">' . get_the_author() . '</span>');
            } elseif (is_day()) {
                printf(__('Day: %s', 'zura'), '<span>' . get_the_date(get_option('date_format')) . '</span>');
            } elseif (is_month()) {
                printf(__('Month: %s', 'zura'), '<span>' . get_the_date(get_option('date_format')) . '</span>');
            } elseif (is_year()) {
                printf(__('Year: %s', 'zura'), '<span>' . get_the_date(get_option('date_format')) . '</span>');
            } elseif (is_tax('post_format', 'post-format-aside')) {
                esc_html_e('Aside', 'zura');
            } elseif (is_tax('post_format', 'post-format-gallery')) {
                esc_html_e('Gallery', 'zura');
            } elseif (is_tax('post_format', 'post-format-image')) {
                esc_html_e('Image', 'zura');
            } elseif (is_tax('post_format', 'post-format-video')) {
                esc_html_e('Video', 'zura');
            } elseif (is_tax('post_format', 'post-format-quote')) {
                esc_html_e('Quote', 'zura');
            } elseif (is_tax('post_format', 'post-format-link')) {
                esc_html_e('Link', 'zura');
            } elseif (is_tax('post_format', 'post-format-status')) {
                esc_html_e('Status', 'zura');
            } elseif (is_tax('post_format', 'post-format-audio')) {
                esc_html_e('Audio', 'zura');
            } elseif (is_tax('post_format', 'post-format-chat')) {
                esc_html_e('Chat', 'zura');
            } else {
                esc_html_e('', 'zura');
            }
        } else {
            the_title();
        }

        return ob_get_clean();
    }

    public static function getBreadcrumb($home_text = 'Home', $delimiter = '-')
    {
        global $post;
        ob_start();

        if (is_front_page()) {
            echo esc_html($home_text);
        } elseif (is_home()) {
            echo esc_html('Blog');
        } else {
            echo '<a href="' . esc_url(home_url('/')) . '">' . $home_text . '</a> ' . $delimiter . ' ';
        }

        if (is_category()) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
            echo '<span class="current">' . single_cat_title(esc_html__('Archive by category: ', 'bears'), false) . '</span>';
        } elseif (is_tag()) {
            echo '<span class="current">' . single_tag_title(esc_html__('Posts tagged: ', 'bears'), false) . '</span>';
        } elseif (is_tax()) {
            echo '<span class="current">' . single_term_title(esc_html__('Archive by taxonomy: ', 'bears'), false) . '</span>';
        } elseif (is_search()) {
            echo '<span class="current">' . esc_html__('Search results for: ', 'bears') . get_search_query() . '</span>';
        } elseif (is_day()) {
            echo '<a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . ' ' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo '<span class="current">' . get_the_time('d') . '</span>';
        } elseif (is_month()) {
            echo '<span class="current">' . get_the_time('F') . ' ' . get_the_time('Y') . '</span>';
        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a href="' . esc_url(home_url('/')) . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
                echo ' ' . $delimiter . ' ' . '<span class="current">' . get_the_title() . '</span>';
            } else {
                $cat = get_the_category();
                $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                echo '' . $cats;
                echo '<span class="current">' . get_the_title() . '</span>';
            }
        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            if ($post_type) echo '<span class="current">' . $post_type->labels->singular_name . '</span>';
        } elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
            echo ' ' . $delimiter . ' ' . '<span class="current">' . get_the_title() . '</span>';
        } elseif (is_page() && !is_front_page() && !$post->post_parent) {
            echo '<span class="current">' . get_the_title() . '</span>';
        } elseif (is_page() && !is_front_page() && $post->post_parent) {
            $parent_id = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo '' . $breadcrumbs[$i];
                if ($i != count($breadcrumbs) - 1)
                    echo ' ' . $delimiter . ' ';
            }
            echo ' ' . $delimiter . ' ' . '<span class="current">' . get_the_title() . '</span>';
        } elseif (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo '<span class="current">' . esc_html__('Articles posted by ', 'bears') . $userdata->display_name . '</span>';
        } elseif (is_404()) {
            echo '<span class="current">' . esc_html__('Error 404', 'bears') . '</span>';
        }

        if (get_query_var('paged')) {
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) echo ' (';
            echo ' ' . $delimiter . ' ' . esc_html__('Page', 'bears') . ' ' . get_query_var('paged');
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) echo ')';
        }

        return ob_get_clean();
    }
}

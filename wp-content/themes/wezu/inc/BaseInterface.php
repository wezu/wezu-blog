<?php
/**
 * @package zura
 */

namespace Zura;


interface BaseInterface
{
    public function register();
}

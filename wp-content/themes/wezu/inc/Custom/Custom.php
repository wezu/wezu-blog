<?php

namespace Zura\Custom;

use Zura\BaseInterface;

/**
 * Custom
 * use it to write your custom functions.
 */
class Custom implements BaseInterface
{
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register()
    {
        add_action('init', array($this, 'registerPostTypes'), 10, 4);
        add_action('after_switch_theme', array($this, 'rewriteFlush'));
    }

    /**
     * Create Custom Post Types
     * @return void registered post type object, or an error object
     */
    public function registerPostTypes()
    {

    }

    /**
     * Flush Rewrite on CPT activation
     * @return mixed
     */
    public function rewriteFlush()
    {
        // Flush the rewrite rules only on theme activation
        flush_rewrite_rules();
    }
}

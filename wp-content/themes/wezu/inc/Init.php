<?php
/**
 * @package zura
 */

namespace Zura;

final class Init
{
    /**
     * Store all the classes inside an array
     * @return array Full list of classes
     */
    public static function getServices()
    {
        return [
            Core\Tags::class,
            Setup\Sidebar::class,
            Setup\Setup::class,
            Setup\Menus::class,
            Setup\Enqueue::class,
            Custom\Custom::class,
            Custom\Extras::class,
            Plugins\Acf::class,
            Plugins\Cpt::class,
            Plugins\ThemeOption::class,
            Plugins\PluginActivation\PluginActivation::class,
        ];
    }

    /**
     * Loop through the classes, initialize them, and call the register() method if it exists
     * @return void
     * @throws \Exception
     */
    public static function registerServices()
    {
        foreach (self::getServices() as $class) {
            $service = self::instantiate($class);
            if (!$service instanceof BaseInterface) {
                throw new \Exception("You class is not instance of " . BaseInterface::class);
            }
            $service->register();
        }
    }

    /**
     * Initialize the class
     * @param mixed $class from the services array
     * @return mixed instance new instance of the class
     */
    private static function instantiate($class)
    {
        return new $class();
    }

    public static function registerHelpers()
    {
        foreach (glob(dirname(__FILE__) . '/Helpers/*.php') as $file) {
            require_once $file;
        }
    }
}

<?php

/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package Zura
 * @subpackage ImZon.Net
 * @since ZuraVN.Com 1.0
 */


/**
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */

if (post_password_required()) {
    return;
}

?>
<div id="comments" class="comments-area">
    <?php if (have_comments()) : ?>
        <div class="st-comments-wrap clearfix">
            <h4 class="comments-title">
                <span><?php comments_number(esc_html__('Comments', 'zura'), esc_html__('1 Comments', 'zura'), esc_html__('Comments (%)', 'zura')); ?></span>
            </h4>
            <ol class="comment-list">
                <?php wp_list_comments('type=comment&callback=zu_comment'); ?>
            </ol>
            <?php zu_comment_nav(); ?>
        </div>
    <?php endif; // have_comments() ?>

    <?php
    //Customer comment form
    $commenter = wp_get_current_commenter();
    $allowed_html = array(
        "span" => array(),
    );
    $req = get_option('require_name__mail');
    $aria_req = ($req ? " aria-required='true'" : '');
    $args = array(
        'id_form' => 'commentform',
        'id_submit' => 'submit',
        'title_reply' => wp_kses(__('<span>Reply Comment</span>', 'zura'), $allowed_html),
        'cancel_reply_link' => esc_html__('Cancel Reply', 'zura'),
        'label_submit' => esc_html__('Post Comment', 'zura'),
        'class_submit' => 'btn btn-primary',
        'comment_notes_before' => '',
        'fields' => apply_filters('comment_form_default_fields', array(
                'author' =>
                    '<p class="comment-form-author">' .
                    '<label for="author">' . esc_html__('Your Name', 'zura') . ' <span class="primary-color">*</span></label>' .
                    '<input id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) .
                    '" size="30"' . esc_attr($aria_req) . ' class="form-control" placeholder="' . esc_html__('Name', 'zura') . '"/></p>',
                'email' =>

                    '<p class="comment-form-email">' .
                    '<label for="email">' . esc_html__('Your Email', 'zura') . ' <span class="primary-color">*</span></label>' .
                    '<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) .
                    '" size="30"' . esc_attr($aria_req) . ' placeholder="' . esc_html__('Email', 'zura') . '"  class="form-control"/></p>',
                'url' =>

                    '<p class="comment-form-url">' .
                    '<label for="url">' . esc_html__('Website', 'zura') . '</label>' .
                    '<input id="url" name="url" type="text" value="' . esc_attr($commenter['comment_author_url']) .
                    '" size="30" class="form-control" placeholder="' . esc_html__('Website', 'zura') . '"/></p>',
            )
        ),
        'comment_field' => '<label for="url">' . esc_html__('Your Comment', 'zura') . '</label><p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" placeholder="' . esc_html__('Comment', 'zura') . '" aria-required="true" class="form-control"></textarea></p>',
    );
    comment_form($args);
    ?>
</div><!-- #comments -->

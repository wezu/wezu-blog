<?php
/**
 * The template for displaying all pages
 *
 * @package zura
 */

get_header(); ?>
    <div id="main-content" class="zu-main-content">
        <!-- <div class="<?php zu_main_css(); ?>"> -->
            <div class="row">
                <div class="col-sm-12">
                    <div id="primary" class="content-area">
                        <?php
                        /* Start the Loop */
                        while (have_posts()) :

                            the_post();

                            get_template_part('views/pages/content');

                            // If comments are open or we have at least one comment, load up the comment template.
                            if (comments_open()) :
                                comments_template();
                            endif;

                        endwhile;
                        ?>
                    </div><!-- #primary -->
                </div><!-- .col- -->
            </div><!-- .row -->
        </div><!-- .container -->
    <!-- </div>#main-content -->
<?php
get_footer();

